from time import sleep, time
from xmlrpc.client import boolean
from serial import Serial, VERSION as PYSERIAL_VERSION, SerialException
from serial.tools.list_ports import comports
from warnings import warn
from math import sqrt

import struct

# Pozyx status returns
POZYX_FAILURE = 0x0
POZYX_SUCCESS = 0x1
POZYX_TIMEOUT = 0x8

class PozyxConstants:
    # Pozyx serial buffer sizes
    MAX_BUF_SIZE = 100
    MAX_SERIAL_SIZE = 28

    # Remote operations
    REMOTE_READ = 0x02
    REMOTE_WRITE = 0x04
    REMOTE_DATA = 0x06
    REMOTE_FUNCTION = 0x08

    # Pozyx delay constants
    DELAY_POLLING = 0.002
    DELAY_POLLING_FAST = 0.0005
    DELAY_LOCAL_WRITE = 0.001
    DELAY_LOCAL_FUNCTION = 0.005
    DELAY_REMOTE_WRITE = 0.005
    DELAY_REMOTE_FUNCTION = 0.01
    DELAY_INTERRUPT = 0.1
    DELAY_RANGING = 0.2
    DELAY_MODE_CHANGE = 0.02
    DELAY_FLASH = 0.5

    # Pozyx timeout constants
    TIMEOUT_RANGING = 0.025
    TIMEOUT_REMOTE_RANGING = 0.1
    TIMEOUT_POSITIONING = 0.2
    TIMEOUT_REMOTE_POSITIONING = 0.4
    TIMEOUT_POSITIONING_DATA = 1.0
    TIMEOUT_REMOTE_POSITIONING_DATA = 1.0
    TIMEOUT_OPTIMAL_DISCOVERY = 0.1

    # Pozyx status returns
    STATUS_FAILURE = POZYX_FAILURE
    STATUS_SUCCESS = POZYX_SUCCESS
    STATUS_TIMEOUT = POZYX_TIMEOUT

    # Pozyx interrupt pin
    INT_PIN0 = POZYX_FAILURE
    INT_PIN1 = POZYX_SUCCESS

    # Pozyx led control indexes
    LED_CTRL_LED_RX = 0x10
    LED_CTRL_LED_TX = 0x20
    LED_ON = True
    LED_OFF = False

    # Pozyx device modes
    TAG_MODE = 0
    ANCHOR_MODE = 1
    ALOHA_MODE = 2

    # The GPIO modes
    GPIO_DIGITAL_INPUT = 0
    GPIO_PUSH_PULL = 1
    GPIO_OPEN_DRAIN = 1

    ALL_GPIO_MODES = [GPIO_DIGITAL_INPUT, GPIO_PUSH_PULL, GPIO_OPEN_DRAIN]

    # The GPIO pull resistor configuration
    GPIO_NO_PULL = 0
    GPIO_PULL_UP = 1
    GPIO_PULL_DOWN = 2

    ALL_GPIO_PULLS = [GPIO_NO_PULL, GPIO_PULL_UP, GPIO_PULL_DOWN]

    # anchor selection modes
    ANCHOR_SELECT_MANUAL = 0
    ANCHOR_SELECT_AUTO = 1

    # discovery options
    DISCOVERY_ANCHORS_ONLY = 0
    DISCOVERY_TAGS_ONLY = 1
    DISCOVERY_ALL_DEVICES = 2

    DISCOVERY_TYPES = [DISCOVERY_ALL_DEVICES, DISCOVERY_ANCHORS_ONLY, DISCOVERY_TAGS_ONLY]

    # Pozyx positioning dimensions
    DIMENSION_3D = 3
    DIMENSION_2D = 2
    DIMENSION_2_5D = 1

    DIMENSIONS = [DIMENSION_3D, DIMENSION_2D, DIMENSION_2_5D]

    # positioning algorithm options
    POSITIONING_ALGORITHM_UWB_ONLY = 0
    POSITIONING_ALGORITHM_TRACKING = 4
    POSITIONING_ALGORITHM_NONE = 3

    POSITIONING_ALGORITHMS = [POSITIONING_ALGORITHM_UWB_ONLY, POSITIONING_ALGORITHM_TRACKING, POSITIONING_ALGORITHM_NONE]

    # ranging protocol options
    RANGE_PROTOCOL_PRECISION = 0x00
    RANGE_PROTOCOL_FAST = 0x01
    RANGE_PROTOCOL_TEST = 0x02

    RANGING_PROTOCOLS = [RANGE_PROTOCOL_PRECISION, RANGE_PROTOCOL_FAST]

    # positioning filters
    FILTER_TYPE_NONE = 0
    FILTER_TYPE_FIR = 1
    FILTER_TYPE_MOVING_AVERAGE = 3
    FILTER_TYPE_MOVING_MEDIAN = 4

    FILTER_TYPES = [FILTER_TYPE_NONE, FILTER_TYPE_FIR, FILTER_TYPE_MOVING_AVERAGE, FILTER_TYPE_MOVING_MEDIAN]

    # how to intercept pozyx events: by polling or by interrupts
    MODE_POLLING = 0
    MODE_INTERRUPT = 1

    # Division factors for converting the raw register values to meaningful
    # physical quantities
    POSITION_DIV_MM = 1.0
    PRESSURE_DIV_PA = 1000.0
    MAX_LINEAR_ACCELERATION_DIV_MG = 1.0
    ACCELERATION_DIV_MG = 1.0
    GYRO_DIV_DPS = 16.0
    MAGNETOMETER_DIV_UT = 16.0
    EULER_ANGLES_DIV_DEG = 16.0
    QUATERNION_DIV = 16384.0
    TEMPERATURE_DIV_CELSIUS = 1.0

    # flash configuration types
    FLASH_SAVE_REGISTERS = 1
    FLASH_SAVE_ANCHOR_IDS = 2
    FLASH_SAVE_NETWORK = 3
    FLASH_SAVE_ALL = 4

    ALL_FLASH_SAVE_TYPES = [FLASH_SAVE_REGISTERS, FLASH_SAVE_ANCHOR_IDS, FLASH_SAVE_NETWORK, FLASH_SAVE_ALL]

    # possible pin configuration settings
    INTERRUPT_CONFIG = 0x24
    PIN_MODE_PUSH_PULL = 0
    PIN_MODE_OPEN_DRAIN = 1

    # Possible pin activity states
    PIN_ACTIVE_LOW = 0
    PIN_ACTIVE_HIGH = 1

    # Possible UWB settings

    UWB_BITRATE_110_KBPS = 0
    UWB_BITRATE_850_KBPS = 1
    UWB_BITRATE_6810_KBPS = 2

    UWB_PRF_16_MHZ = 1
    UWB_PRF_64_MHZ = 2

    UWB_PLEN_64 = 0x04
    UWB_PLEN_128 = 0x14
    UWB_PLEN_256 = 0x24
    UWB_PLEN_512 = 0x34
    UWB_PLEN_1024 = 0x08
    UWB_PLEN_1536 = 0x18
    UWB_PLEN_2048 = 0x28
    UWB_PLEN_4096 = 0x0C

    ALL_UWB_CHANNELS = [1, 2, 3, 4, 5, 7]

    ALL_UWB_BITRATES = [
        UWB_BITRATE_110_KBPS,
        UWB_BITRATE_850_KBPS,
        UWB_BITRATE_6810_KBPS,
    ]

    ALL_UWB_PRFS = [
        UWB_PRF_16_MHZ,
        UWB_PRF_64_MHZ,
    ]

    ALL_UWB_PLENS = [
        UWB_PLEN_64,
        UWB_PLEN_128,
        UWB_PLEN_256,
        UWB_PLEN_512,
        UWB_PLEN_1024,
        UWB_PLEN_1536,
        UWB_PLEN_2048,
        UWB_PLEN_4096,
    ]

    # ALOHA send types
    ALOHA_SEND_CUSTOM_QUEUED = 0
    ALOHA_SEND_CUSTOM_IMMEDIATE = 1
    ALOHA_SEND_IMMEDIATE = 2

    ALL_ALOHA_TYPES = [ALOHA_SEND_CUSTOM_QUEUED, ALOHA_SEND_CUSTOM_IMMEDIATE, ALOHA_SEND_IMMEDIATE]

# Pozyx firmware identifiers
POZYX_FW_MAJOR = 0xF0
POZYX_FW_MINOR = 0xF

# Pozyx device identifier for hardware
POZYX_ANCHOR = 0x00
POZYX_TAG = 0x20

# Pozyx serial buffer sizes
MAX_BUF_SIZE = 100
MAX_SERIAL_SIZE = 28

# Pozyx delay constants
POZYX_DELAY_POLLING = 0.001
POZYX_DELAY_LOCAL_WRITE = 0.001
POZYX_DELAY_LOCAL_FUNCTION = 0.005
POZYX_DELAY_REMOTE_WRITE = 0.005
POZYX_DELAY_REMOTE_FUNCTION = 0.01
POZYX_DELAY_INTERRUPT = 0.1
POZYX_DELAY_CALIBRATION = 1
POZYX_DELAY_MODE_CHANGE = 0.02
POZYX_DELAY_RANGING = 0.025
POZYX_DELAY_REMOTE_RANGING = 0.1
POZYX_DELAY_POSITIONING = 0.2
POZYX_DELAY_REMOTE_POSITIONING = 0.4
POZYX_DELAY_FLASH = 0.5

# Pozyx positioning dimensions
POZYX_3D = 3
POZYX_2D = 2
POZYX_2_5D = 1

# Pozyx interrupt pin
POZYX_INT_PIN0 = 0x0
POZYX_INT_PIN1 = 0x1

# Pozyx led control indexes
POZYX_LED_CTRL_LEDRX = 0x10
POZYX_LED_CTRL_LEDTX = 0x20
POZYX_LED_ON = True
POZYX_LED_OFF = False

# Pozyx device modes
POZYX_ANCHOR_MODE = 0
POZYX_TAG_MODE = 1

# The GPIO modes
POZYX_GPIO_DIGITAL_INPUT = 0
POZYX_GPIO_PUSHPULL = 1
POZYX_GPIO_OPENDRAIN = 1

# The GPIO pull resistor configuration
POZYX_GPIO_NOPULL = 0
POZYX_GPIO_PULLUP = 1
POZYX_GPIO_PULLDOWN = 2

# anchor selection modes
POZYX_ANCHOR_SEL_MANUAL = 0
POZYX_ANCHOR_SEL_AUTO = 1

# discovery options
POZYX_DISCOVERY_ANCHORS_ONLY = 0
POZYX_DISCOVERY_TAGS_ONLY = 1
POZYX_DISCOVERY_ALL_DEVICES = 2

# positioning algorithm options
POZYX_POS_ALG_UWB_ONLY = 0
POZYX_POS_ALG_TRACKING = 4

# ranging protocol options
POZYX_RANGE_PROTOCOL_PRECISION = 0x00
POZYX_RANGE_PROTOCOL_FAST = 0x01
POZYX_RANGE_PROTOCOL_TEST = 0x02

# positioning filters
FILTER_TYPE_NONE = 0
FILTER_TYPE_FIR = 1
FILTER_TYPE_MOVINGAVERAGE = 3
FILTER_TYPE_MOVINGMEDIAN = 4

# how to intercept pozyx events: by polling or by interrupts
MODE_POLLING = 0
MODE_INTERRUPT = 1

# Division factors for converting the raw register values to meaningful
# physical quantities
POZYX_POS_DIV_MM = 1.0
POZYX_PRESS_DIV_PA = 1000.0
POZYX_MAX_LIN_ACCEL_DIV_MG = 1.0
POZYX_ACCEL_DIV_MG = 1.0
POZYX_GYRO_DIV_DPS = 16.0
POZYX_MAG_DIV_UT = 16.0
POZYX_EULER_DIV_DEG = 16.0
POZYX_QUAT_DIV = 16384.0
POZYX_TEMP_DIV_CELSIUS = 1.0

# flash configuration types
POZYX_FLASH_REGS = 1
POZYX_FLASH_ANCHOR_IDS = 2
POZYX_FLASH_NETWORK = 3
POZYX_FLASH_ALL = 4

# ALOHA send types
POZYX_QUEUE_CUSTOM_ALOHA = 0
POZYX_SEND_CUSTOM_ALOHA_IMMEDIATE = 1
POZYX_SEND_ALOMA_IMMEDIATE = 2

# possible pin configuration settings
POZYX_INT_CONFIG = 0x24
PIN_MODE_PUSHPULL = 0
PIN_MODE_OPENDRAIN = 1

PIN_ACTIVE_LOW = 0
PIN_ACTIVE_HIGH = 1

POZYX_ALL_CHANNELS = [1, 2, 3, 4, 5, 7]
POZYX_ALL_BITRATES = [0, 1, 2]
POZYX_ALL_PRFS = [1, 2]
POZYX_ALL_PLENS = [0x04, 0x14, 0x24, 0x34, 0x08, 0x18, 0x28, 0x0C]

class PozyxErrorCodes:
    POZYX_ERROR_NONE = 0x00
    POZYX_ERROR_I2C_WRITE = 0x01
    POZYX_ERROR_I2C_CMDFULL = 0x02
    POZYX_ERROR_ANCHOR_ADD = 0x03
    POZYX_ERROR_COMM_QUEUE_FULL = 0x04
    POZYX_ERROR_I2C_READ = 0x05
    POZYX_ERROR_UWB_CONFIG = 0x06
    POZYX_ERROR_OPERATION_QUEUE_FULL = 0x07
    POZYX_ERROR_TDMA = 0xA0
    POZYX_ERROR_STARTUP_BUSFAULT = 0x08
    POZYX_ERROR_FLASH_INVALID = 0x09
    POZYX_ERROR_NOT_ENOUGH_ANCHORS = 0x0A
    POZYX_ERROR_DISCOVERY = 0X0B
    POZYX_ERROR_CALIBRATION = 0x0C
    POZYX_ERROR_FUNC_PARAM = 0x0D
    POZYX_ERROR_ANCHOR_NOT_FOUND = 0x0E
    POZYX_ERROR_FLASH = 0x0F
    POZYX_ERROR_MEMORY = 0x10
    POZYX_ERROR_RANGING = 0x11
    POZYX_ERROR_RTIMEOUT1 = 0x12
    POZYX_ERROR_RTIMEOUT2 = 0x13
    POZYX_ERROR_TXLATE = 0x14
    POZYX_ERROR_UWB_BUSY = 0x15
    POZYX_ERROR_POSALG = 0x16
    POZYX_ERROR_NOACK = 0x17
    POZYX_ERROR_SNIFF_OVERFLOW = 0xE0
    POZYX_ERROR_NO_PPS = 0xF0
    POZYX_ERROR_NEW_TASK = 0xF1
    POZYX_ERROR_UNRECDEV = 0xFE
    POZYX_ERROR_GENERAL = 0xFF

# error-code definitions
POZYX_ERROR_NONE = 0x00
POZYX_ERROR_I2C_WRITE = 0x01
POZYX_ERROR_I2C_CMDFULL = 0x02
POZYX_ERROR_ANCHOR_ADD = 0x03
POZYX_ERROR_COMM_QUEUE_FULL = 0x04
POZYX_ERROR_I2C_READ = 0x05
POZYX_ERROR_UWB_CONFIG = 0x06
POZYX_ERROR_OPERATION_QUEUE_FULL = 0x07
POZYX_ERROR_TDMA = 0xA0
POZYX_ERROR_STARTUP_BUSFAULT = 0x08
POZYX_ERROR_FLASH_INVALID = 0x09
POZYX_ERROR_NOT_ENOUGH_ANCHORS = 0x0A
POZYX_ERROR_DISCOVERY = 0X0B
POZYX_ERROR_CALIBRATION = 0x0C
POZYX_ERROR_FUNC_PARAM = 0x0D
POZYX_ERROR_ANCHOR_NOT_FOUND = 0x0E
POZYX_ERROR_FLASH = 0x0F
POZYX_ERROR_MEMORY = 0x10
POZYX_ERROR_RANGING = 0x11
POZYX_ERROR_RTIMEOUT1 = 0x12
POZYX_ERROR_RTIMEOUT2 = 0x13
POZYX_ERROR_TXLATE = 0x14
POZYX_ERROR_UWB_BUSY = 0x15
POZYX_ERROR_POSALG = 0x16
POZYX_ERROR_NOACK = 0x17
POZYX_ERROR_SNIFF_OVERFLOW = 0xE0
POZYX_ERROR_NO_PPS = 0xF0
POZYX_ERROR_NEW_TASK = 0xF1
POZYX_ERROR_UNRECDEV = 0xFE
POZYX_ERROR_GENERAL = 0xFF

ERROR_CODES = {
    PozyxErrorCodes.POZYX_ERROR_NONE: "NO ERROR",
    PozyxErrorCodes.POZYX_ERROR_I2C_WRITE: "ERROR 0x01: Error writing to a register through the I2C bus",
    PozyxErrorCodes.POZYX_ERROR_I2C_CMDFULL: "ERROR 0x02: Pozyx cannot handle all the I2C commands at once",
    PozyxErrorCodes.POZYX_ERROR_ANCHOR_ADD: "ERROR 0x03: Cannot add anchor to the internal device list",
    PozyxErrorCodes.POZYX_ERROR_COMM_QUEUE_FULL: "ERROR 0x04: Communication queue is full, too many UWB messages",
    PozyxErrorCodes.POZYX_ERROR_I2C_READ: "ERROR 0x05: Error reading from a register from the I2C bus",
    PozyxErrorCodes.POZYX_ERROR_UWB_CONFIG: "ERROR 0x06: Cannot change the UWB configuration",
    PozyxErrorCodes.POZYX_ERROR_OPERATION_QUEUE_FULL: "ERROR 0x07: Pozyx cannot handle all the operations at once",
    PozyxErrorCodes.POZYX_ERROR_STARTUP_BUSFAULT: "ERROR 0x08: Internal bus error",
    PozyxErrorCodes.POZYX_ERROR_FLASH_INVALID: "ERROR 0x09: Flash memory is corrupted or invalid",
    PozyxErrorCodes.POZYX_ERROR_NOT_ENOUGH_ANCHORS: "ERROR 0x0A: Not enough anchors available for positioning",
    PozyxErrorCodes.POZYX_ERROR_DISCOVERY: "ERROR 0x0B: Error during the Discovery process",
    PozyxErrorCodes.POZYX_ERROR_CALIBRATION: "ERROR 0x0C: Error during the auto calibration process",
    PozyxErrorCodes.POZYX_ERROR_FUNC_PARAM: "ERROR 0x0D: Invalid function parameters for the register function",
    PozyxErrorCodes.POZYX_ERROR_ANCHOR_NOT_FOUND: "ERROR 0x0E: The coordinates of an anchor are not found",
    PozyxErrorCodes.POZYX_ERROR_FLASH: "ERROR 0x0F: Flash error",
    PozyxErrorCodes.POZYX_ERROR_MEMORY: "ERROR 0x10: Memory error",
    PozyxErrorCodes.POZYX_ERROR_RANGING: "ERROR 0x11: Ranging failed",
    PozyxErrorCodes.POZYX_ERROR_RTIMEOUT1: "ERROR 0x12: Ranging timeout",
    PozyxErrorCodes.POZYX_ERROR_RTIMEOUT2: "ERROR 0x13: Ranging timeout",
    PozyxErrorCodes.POZYX_ERROR_TXLATE: "ERROR 0x14: Tx was late",
    PozyxErrorCodes.POZYX_ERROR_UWB_BUSY: "ERROR 0x15: UWB is busy",
    PozyxErrorCodes.POZYX_ERROR_POSALG: "ERROR 0x16: Positioning failed",
    PozyxErrorCodes.POZYX_ERROR_NOACK: "ERROR 0x17: No Acknowledge received",
    PozyxErrorCodes.POZYX_ERROR_NEW_TASK: "ERROR 0xF1: Cannot create task",
    PozyxErrorCodes.POZYX_ERROR_UNRECDEV: "ERROR 0xFE: Hardware not recognized. Please contact support@pozyx.io",
    PozyxErrorCodes.POZYX_ERROR_GENERAL: "ERROR 0xFF: General error",
}

ERROR_MESSAGES = ERROR_CODES

class PozyxBitmasks:
    # Pozyx firmware identifiers
    FIRMWARE_MAJOR = 0xF0
    FIRMWARE_MINOR = 0xF

    # Pozyx device identifier for hardware
    ANCHOR = 0x00
    TAG = 0x20

    # Bit mask for POZYX_ST_RESULT
    SELFTEST_RESULT_ACCELEROMETER = 0x01
    SELFTEST_RESULT_MAGNETOMETER = 0x02
    SELFTEST_RESULT_GYRO = 0x04
    SELFTEST_RESULT_MCU = 0x08
    SELFTEST_RESULT_PRESSURE = 0x10
    SELFTEST_RESULT_UWB = 0x20

    # Bit mask for POZYX_INT_STATUS
    INT_STATUS_ERR = 0x01
    INT_STATUS_POS = 0x02
    INT_STATUS_IMU = 0x04
    INT_STATUS_RX_DATA = 0x08
    INT_STATUS_FUNC = 0x10

    # Bit mask for POZYX_INT_MASK
    INT_MASK_ERR = 0x01
    INT_MASK_POS = 0x02
    INT_MASK_IMU = 0x04
    INT_MASK_RX_DATA = 0x08
    INT_MASK_FUNC = 0x10
    INT_MASK_TDMA = 0x40
    INT_MASK_PIN = 0x80
    INT_MASK_ALL = 0x1F

    # Bit mask for POZYX_LED_CTRL
    LED_CTRL_LED1 = 0x01
    LED_CTRL_LED2 = 0x02
    LED_CTRL_LED3 = 0x04
    LED_CTRL_LED4 = 0x08

    # Bit mask for device type
    DEVICE_TYPE = 0xE0

# Bit mask for POZYX_ST_RESULT
POZYX_ST_RESULT_ACC = 0x01
POZYX_ST_RESULT_MAGN = 0x02
POZYX_ST_RESULT_GYR = 0x04
POZYX_ST_RESULT_MCU = 0x08
POZYX_ST_RESULT_PRES = 0x10
POZYX_ST_RESULT_UWB = 0x20

# Bit mask for POZYX_INT_STATUS
POZYX_INT_STATUS_ERR = 0x01
POZYX_INT_STATUS_POS = 0x02
POZYX_INT_STATUS_IMU = 0x04
POZYX_INT_STATUS_RX_DATA = 0x08
POZYX_INT_STATUS_FUNC = 0x10

# Bit mask for POZYX_INT_MASK
POZYX_INT_MASK_ERR = 0x01
POZYX_INT_MASK_POS = 0x02
POZYX_INT_MASK_IMU = 0x04
POZYX_INT_MASK_RX_DATA = 0x08
POZYX_INT_MASK_FUNC = 0x10
POZYX_INT_MASK_TDMA = 0x40
POZYX_INT_MASK_PIN = 0x80
POZYX_INT_MASK_ALL = 0x1F

# Bit mask for POZYX_LED_CTRL
POZYX_LED_CTRL_LED1 = 0x01
POZYX_LED_CTRL_LED2 = 0x02
POZYX_LED_CTRL_LED3 = 0x04
POZYX_LED_CTRL_LED4 = 0x08

# Remote operations
POZYX_REMOTE_READ = 0x02
POZYX_REMOTE_WRITE = 0x04
POZYX_REMOTE_DATA = 0x06
POZYX_REMOTE_FUNCTION = 0x08

# Bit mask for device type
POZYX_TYPE = 0xE0

class PozyxRegisters:
    WHO_AM_I = 0x0  # Returns the constant value 0x43.
    FIRMWARE_VERSION = 0x1  # Returns the POZYX firmware version.
    HARDWARE_VERSION = 0x2  # Returns the POZYX hardware version.
    SELFTEST_RESULT = 0x3  # Returns the self-test result
    ERROR_CODE = 0x4  # Describes a possibly system error.
    INTERRUPT_STATUS = 0x5  # Indicates the source of the interrupt.
    CALIBRATION_STATUS = 0x6  # Returns the calibration status.

    # Configuration registers
    INTERRUPT_MASK = 0x10  # Indicates which interrupts are enabled.
    INTERRUPT_PIN = 0x11  # Configure the interrupt pin
    POSITIONING_FILTER = 0x14  # Filter used for positioning
    LED_CONFIGURATION = 0x15  # Configure the LEDs
    POSITIONING_ALGORITHM = 0x16  # Algorithm used for positioning
    # Configure the number of anchors and selection procedure
    POSITIONING_NUMBER_OF_ANCHORS = 0x17
    ALL_POSITIONING_REGISTERS = [0x14, 0x16, 0x21, 0x38]
    # Defines the update interval in ms in continuous positioning.
    POSITIONING_INTERVAL = 0x18
    NETWORK_ID = 0x1A  # The network id.
    UWB_CHANNEL = 0x1C  # UWB channel number.
    # Configure the UWB datarate and pulse repetition frequency (PRF)
    UWB_RATES = 0x1D
    UWB_PLEN = 0x1E  # Configure the UWB preamble length.
    UWB_GAIN = 0x1F  # Configure the power gain for the UWB transmitter
    ALL_UWB_REGISTERS = [0x1C, 0x1D, 0x1E, 0x1F]
    UWB_CRYSTAL_TRIM = 0x20  # Trimming value for the uwb crystal.
    RANGING_PROTOCOL = 0x21  # The ranging protocol
    # Configure the mode of operation of the pozyx device
    OPERATION_MODE = 0x22
    SENSORS_MODE = 0x23  # Configure the mode of operation of the sensors
    CONFIG_BLINK_PAYLOAD = 0x24  # Configure payload that needs to be transmitted with ALOHA
    ALOHA_VARIATION = 0x26  # Configure the variation on the ALOHA
    CONFIG_GPIO_1 = 0x27  # Configure GPIO pin 1.
    CONFIG_GPIO_2 = 0x28  # Configure GPIO pin 2.
    CONFIG_GPIO_3 = 0x29  # Configure GPIO pin 3.
    CONFIG_GPIO_4 = 0x2A  # Configure GPIO pin 4.

    ALL_ALOHA_REGISTERS = [0x18, 0x19, 0x24, 0x25, 0x26]


    # Positioning data
    POSITION_X = 0x30  # x-coordinate of the device in mm.
    POSITION_Y = 0x34  # y-coordinate of the device in mm.
    POSITION_Z = 0x38  # z-coordinate of the device in mm.
    HEIGHT = 0x38  # z-coordinate of the device in mm.
    POSITIONING_ERROR_X = 0x3C  # estimated error covariance of x
    POSITIONING_ERROR_Y = 0x3E  # estimated error covariance of y
    POSITIONING_ERROR_Z = 0x40  # estimated error covariance of z
    POSITIONING_ERROR_XY = 0x42  # estimated covariance of xy
    POSITIONING_ERROR_XZ = 0x44  # estimated covariance of xz
    POSITIONING_ERROR_YZ = 0x46  # estimated covariance of yz

    # Sensor data
    MAX_LINEAR_ACCELERATION = 0x4E  # Return the max linear acceleration and reset it to 0
    PRESSURE = 0x50  # Pressure data in mPa
    ACCELERATION_X = 0x54  # Accelerometer data (in mg)
    ACCELERATION_Y = 0x56
    ACCELERATION_Z = 0x58
    MAGNETIC_X = 0x5A  # Magnemtometer data
    MAGNETIC_Y = 0x5C
    MAGNETIC_Z = 0x5E
    GYRO_X = 0x60  # Gyroscope data
    GYRO_Y = 0x62
    GYRO_Z = 0x64
    # Euler angles heading (or yaw)  (1 degree = 16 LSB )
    EULER_ANGLE_HEADING = 0x66
    EULER_ANGLE_YAW = 0x66
    EULER_ANGLE_ROLL = 0x68  # Euler angles roll ( 1 degree = 16 LSB )
    EULER_ANGLE_PITCH = 0x6A  # Euler angles pitch ( 1 degree = 16 LSB )
    QUATERNION_W = 0x6C  # Weight of quaternion.
    QUATERNION_X = 0x6E  # x of quaternion
    QUATERNION_Y = 0x70  # y of quaternion
    QUATERNION_Z = 0x72  # z of quaternion
    LINEAR_ACCELERATION_X = 0x74  # Linear acceleration in x-direction
    LINEAR_ACCELERATION_Y = 0x76  # Linear acceleration in y-direction
    LINEAR_ACCELERATION_Z = 0x78  # Linear acceleration in z-direction
    GRAVITY_VECTOR_X = 0x7A  # x-component of gravity vector
    GRAVITY_VECTOR_Y = 0x7C  # y-component of gravity vector
    GRAVITY_VECTOR_Z = 0x7E  # z-component of gravity vector
    TEMPERATURE = 0x80  # Temperature

    # General data
    # Returns the number of devices stored internally
    DEVICE_LIST_SIZE = 0x81
    RX_NETWORK_ID = 0x82  # The network id of the latest received message
    RX_DATA_LENGTH = 0x84  # The length of the latest received message
    GPIO_1 = 0x85  # Value of the GPIO pin 1
    GPIO_2 = 0x86  # Value of the GPIO pin 2
    GPIO_3 = 0x87  # Value of the GPIO pin 3
    GPIO_4 = 0x88  # Value of the GPIO pin 4
    BLINK_INDEX = 0x89  # The blink index of the ALOHA transmissions

    # Functions
    RESET_SYSTEM = 0xB0  # Reset the Pozyx device
    LED_CONTROL = 0xB1  # Control LEDS 1 to 4 on the board
    WRITE_TX_DATA = 0xB2  # Write data in the UWB transmit (TX) buffer
    SEND_TX_DATA = 0xB3  # Transmit the TX buffer to some other pozyx device
    READ_RX_DATA = 0xB4  # Read data from the UWB receive (RX) buffer
    DO_RANGING = 0xB5  # Initiate ranging measurement
    DO_POSITIONING = 0xB6  # Initiate the positioning process.
    # Set the list of anchor ID's used for positioning.
    SET_POSITIONING_ANCHOR_IDS = 0xB7
    # Read the list of anchor ID's used for positioning.
    GET_POSITIONING_ANCHOR_IDS = 0xB8
    RESET_FLASH_MEMORY = 0xB9  # Reset a portion of the configuration in flash memory
    SAVE_FLASH_MEMORY = 0xBA  # Store a portion of the configuration in flash memory
    GET_FLASH_DETAILS = 0xBB  # Return information on what is stored in flash
    DO_ALOHA = 0xBC  # Start / stop ALOHA mode

    # Device list functions
    # Get all the network IDs's of devices in the device list.
    GET_DEVICE_LIST_IDS = 0xC0
    # Obtain the network ID's of all pozyx devices within range.
    DO_DISCOVERY = 0xC1

    CLEAR_DEVICES = 0xC3  # Clear the list of all pozyx devices.
    ADD_DEVICE = 0xC4  # Add a pozyx device to the devices list
    # Get the stored device information for a given pozyx device
    GET_DEVICE_INFO = 0xC5
    # Get the stored coordinates of a given pozyx device
    GET_DEVICE_COORDINATES = 0xC6
    # Get the stored range information of a given pozyx device
    GET_DEVICE_RANGE_INFO = 0xC7
    CIR_DATA = 0xC8  # Get the channel impulse response (CIR) coefficients

    DO_POSITIONING_WITH_DATA = 0xCC

# Status registers
POZYX_WHO_AM_I = 0x0  # Returns the constant value 0x43.
POZYX_FIRMWARE_VER = 0x1  # Returns the POZYX firmware version.
POZYX_HARDWARE_VER = 0x2  # Returns the POZYX hardware version.
POZYX_ST_RESULT = 0x3  # Returns the self-test result
POZYX_ERRORCODE = 0x4  # Describes a possibly system error.
POZYX_INT_STATUS = 0x5  # Indicates the source of the interrupt.
POZYX_CALIB_STATUS = 0x6  # Returns the calibration status.

# Configuration registers
POZYX_INT_MASK = 0x10  # Indicates which interrupts are enabled.
POZYX_INT_CONFIG = 0x11  # Configure the interrupt pin
POZYX_CONFIG_LEDS = 0x15  # Configure the LEDs
POZYX_POS_FILTER = 0x14  # Filter used for positioning
POZYX_POS_ALG = 0x16  # Algorithm used for positioning
# Configure the number of anchors and selection procedure
POZYX_POS_NUM_ANCHORS = 0x17
# Defines the update interval in ms in continuous positioning.
POZYX_POS_INTERVAL = 0x18
POZYX_NETWORK_ID = 0x1A  # The network id.
POZYX_UWB_CHANNEL = 0x1C  # UWB channel number.
# Configure the UWB datarate and pulse repetition frequency (PRF)
POZYX_UWB_RATES = 0x1D
POZYX_UWB_PLEN = 0x1E  # Configure the UWB preamble length.
POZYX_UWB_GAIN = 0x1F  # Configure the power gain for the UWB transmitter
POZYX_UWB_XTALTRIM = 0x20  # Trimming value for the uwb crystal.
POZYX_RANGE_PROTOCOL = 0x21  # The ranging protocol
# Configure the mode of operation of the pozyx device
POZYX_OPERATION_MODE = 0x22
POZYX_SENSORS_MODE = 0x23  # Configure the mode of operation of the sensors
POZYX_CONFIG_BLINK_PAYLOAD = 0x24  # Configure payload that needs to be  transmitted with ALOHA
POZYX_ALOHA_VARIATION = 0x26  # Configure the variation on the ALOHA
POZYX_CONFIG_GPIO1 = 0x27  # Configure GPIO pin 1.
POZYX_CONFIG_GPIO2 = 0x28  # Configure GPIO pin 2.
POZYX_CONFIG_GPIO3 = 0x29  # Configure GPIO pin 3.
POZYX_CONFIG_GPIO4 = 0x2A  # Configure GPIO pin 4.

# Positioning data
POZYX_POS_X = 0x30  # x-coordinate of the device in mm.
POZYX_POS_Y = 0x34  # y-coordinate of the device in mm.
POZYX_POS_Z = 0x38  # z-coordinate of the device in mm.
POZYX_POS_ERR_X = 0x3C  # estimated error covariance of x
POZYX_POS_ERR_Y = 0x3E  # estimated error covariance of y
POZYX_POS_ERR_Z = 0x40  # estimated error covariance of z
POZYX_POS_ERR_XY = 0x42  # estimated covariance of xy
POZYX_POS_ERR_XZ = 0x44  # estimated covariance of xz
POZYX_POS_ERR_YZ = 0x46  # estimated covariance of yz

# Sensor data
POZYX_MAX_LIN_ACC = 0x4E  # Return the max linear acceleration and reset it to 0
POZYX_PRESSURE = 0x50  # Pressure data in mPa
POZYX_ACCEL_X = 0x54  # Accelerometer data (in mg)
POZYX_ACCEL_Y = 0x56
POZYX_ACCEL_Z = 0x58
POZYX_MAGN_X = 0x5A  # Magnemtometer data
POZYX_MAGN_Y = 0x5C
POZYX_MAGN_Z = 0x5E
POZYX_GYRO_X = 0x60  # Gyroscope data
POZYX_GYRO_Y = 0x62
POZYX_GYRO_Z = 0x64
# Euler angles heading (or yaw)  (1 degree = 16 LSB )
POZYX_EUL_HEADING = 0x66
POZYX_EUL_ROLL = 0x68  # Euler angles roll ( 1 degree = 16 LSB )
POZYX_EUL_PITCH = 0x6A  # Euler angles pitch ( 1 degree = 16 LSB )
POZYX_QUAT_W = 0x6C  # Weight of quaternion.
POZYX_QUAT_X = 0x6E  # x of quaternion
POZYX_QUAT_Y = 0x70  # y of quaternion
POZYX_QUAT_Z = 0x72  # z of quaternion
POZYX_LIA_X = 0x74  # Linear acceleration in x-direction
POZYX_LIA_Y = 0x76  # Linear acceleration in y-direction
POZYX_LIA_Z = 0x78  # Linear acceleration in z-direction
POZYX_GRAV_X = 0x7A  # x-component of gravity vector
POZYX_GRAV_Y = 0x7C  # y-component of gravity vector
POZYX_GRAV_Z = 0x7E  # z-component of gravity vector
POZYX_TEMPERATURE = 0x80  # Temperature

# General data
# Returns the number of devices stored internally
POZYX_DEVICE_LIST_SIZE = 0x81
POZYX_RX_NETWORK_ID = 0x82  # The network id of the latest received message
POZYX_RX_DATA_LEN = 0x84  # The length of the latest received message
POZYX_GPIO1 = 0x85  # Value of the GPIO pin 1
POZYX_GPIO2 = 0x86  # Value of the GPIO pin 2
POZYX_GPIO3 = 0x87  # Value of the GPIO pin 3
POZYX_GPIO4 = 0x88  # Value of the GPIO pin 4
POZYX_BLINK_INDEX = 0x89  # The blink index of the ALOHA transmissions

# Functions
POZYX_RESET_SYS = 0xB0  # Reset the Pozyx device
POZYX_LED_CTRL = 0xB1  # Control LEDS 1 to 4 on the board
POZYX_TX_DATA = 0xB2  # Write data in the UWB transmit (TX) buffer
POZYX_TX_SEND = 0xB3  # Transmit the TX buffer to some other pozyx device
POZYX_RX_DATA = 0xB4  # Read data from the UWB receive (RX) buffer
POZYX_DO_RANGING = 0xB5  # Initiate ranging measurement
POZYX_DO_POSITIONING = 0xB6  # Initiate the positioning process.
# Set the list of anchor ID's used for positioning.
POZYX_POS_SET_ANCHOR_IDS = 0xB7
# Read the list of anchor ID's used for positioning.
POZYX_POS_GET_ANCHOR_IDS = 0xB8
POZYX_FLASH_RESET = 0xB9  # Reset a portion of the configuration in flash memory
POZYX_FLASH_SAVE = 0xBA  # Store a portion of the configuration in flash memory
POZYX_FLASH_DETAILS = 0xBB  # Return information on what is stored in flash
POZYX_DO_ALOHA = 0xBC

# Device list functions
# Get all the network IDs's of devices in the device list.
POZYX_DEVICES_GETIDS = 0xC0
# Obtain the network ID's of all pozyx devices within range.
POZYX_DEVICES_DISCOVER = 0xC1
# Obtain the coordinates of the pozyx (anchor) devices within range.
POZYX_DEVICES_CALIBRATE = 0xC2
POZYX_DEVICES_CLEAR = 0xC3  # Clear the list of all pozyx devices.
POZYX_DEVICE_ADD = 0xC4  # Add a pozyx device to the devices list
# Get the stored device information for a given pozyx device
POZYX_DEVICE_GETINFO = 0xC5
# Get the stored coordinates of a given pozyx device
POZYX_DEVICE_GETCOORDS = 0xC6
# Get the stored range inforamation of a given pozyx device
POZYX_DEVICE_GETRANGEINFO = 0xC7
POZYX_CIR_DATA = 0xC8  # Get the channel impulse response (CIR) coefficients

class ByteStructure(object):
    """
    The ByteStructure class is the base class that all custom structs inherit
    their basic functionality from. It implements the low-level functionality
    that makes it easy to make use arbitrary struct formats in the interface
    with Pozyx.
    """
    byte_size = 4
    data_format = 'BBBB'

    def __init__(self):
        """Initializes the structures byte data and data arrays"""
        self.byte_data = '00' * self.byte_size
        self.bytes_to_data()

    def load_bytes(self, byte_data):
        """Loads a hex byte array in the structure's data"""
        self.byte_data = byte_data
        self.bytes_to_data()

    def bytes_to_data(self):
        """Transforms hex data into packed UINT8 byte values"""
        self.load_packed(bytes.fromhex(''.join(self.byte_data[i:i+2] for i in range(0, len(self.byte_data), 2))))


    def load_packed(self, packed):
        """Unpacks the packed UINT8 bytes in their right format as given in data_format"""
        self.load([struct.unpack(fmt, packed[i:i+struct.calcsize(fmt)])[0] for i, fmt in enumerate(self.data_format)])

    def load_hex_string(self):
        """Loads the data's hex string for sending"""
        self.byte_data = ''.join(f'{byte:0>2x}' for byte in self.transform_to_bytes())

    def transform_to_bytes(self):
        """Transforms the data to a UINT8 bytestring in hex"""
        return self.transform_data(''.join(['B' * struct.calcsize(fmt) for fmt in self.data_format]))


    def set_packed_size(self):
        """Sets the size (bytesize) to the structures data format, but packed"""
        self.byte_size = 1 * len(''.join(['B' * struct.calcsize(fmt) for fmt in self.data_format]))

    def set_unpacked_size(self):
        """Sets the size (bytesize) to the structures data format, unpacked"""
        self.byte_size = struct.calcsize(self.data_format)

    def transform_data(self, new_format):
        """Transforms the data to a new format, handy for decoding to bytes"""
        return list(struct.unpack(new_format, b''.join(struct.pack(fmt, value) for fmt, value in zip(self.data_format, self.data))))


    def load(self, data, convert=True):
        """Loads data in its relevant class components."""
        raise NotImplementedError('load(data) should be customised for every derived structure')

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = value

    def __len__(self):
        """Returns the size of the structure's data"""
        return len(self.data)

    def __eq__(self, other):
        """Returns whether the structures contain the same data"""
        return False if (other is None) else (self.data == other.data and self.data_format == other.data_format)

    def __str__(self):
        """Returns a string that should be tailored to each ByteStructure-derived class."""
        return ', '.join(map(str, self.data))

def is_reg_readable(reg):
    """Returns whether a Pozyx register is readable."""
    return True if (0x00 <= reg < 0x07) or (0x10 <= reg < 0x12) or (0x14 <= reg < 0x22) or (0x22 <= reg <= 0x24) or (
            0x26 <= reg < 0x2B) or (0x30 <= reg < 0x48) or (0x4E <= reg < 0x89)else False

def is_reg_writable(reg):
    """Returns whether a Pozyx register is writeable."""
    return True if (0x10 <= reg < 0x12) or (0x14 <= reg < 0x22) or (0x22 <= reg <= 0x24) or (0x26 <= reg < 0x2B) or (
            0x30 <= reg < 0x3C) or (0x85 <= reg < 0x89) else False

def is_functioncall(reg):
    """Returns whether a Pozyx register is a Pozyx function."""
    return True if (0xB0 <= reg <= 0xBC) or (0xC0 <= reg < 0xC9) else False

def dataCheck(data):
    """Returns whether an object is part of the ByteStructure-derived classes or not.

    The function checks the base classes of the passed data object. This function enables
    many library functions to be passed along its data as either an int/list or the properly
    intended data structure. For example, the following code will result in the
    same behaviour::

      >>> p.setCoordinates([0, 0, 0])
      >>> # or
      >>> coords = Coordinates()
      >>> p.setCoordinates(coords)

    AND

      >>> p.setNetworkId(0x6000)
      >>> # or
      >>> n = NetworkID(0x6000)
      >>> p.setNetworkId(n)

    Note that this only works for functions where you change one of the Pozyx's
    settings. When reading data from the Pozyx, you have to pass along the correct
    data structure.

    Using dataCheck:
    You might want to use this in your own function, as it makes it more robust
    to whether an int or list gets sent as a parameter to your function, or a
    ByteStructure-like object. If so, you can perform::

      >>> if not dataCheck(sample): # assume a is an int but you want it to be a SingleRegister
      >>>     sample = SingleRegister(sample)

    """
    return False if not(
        Data in type(data).__bases__ or ByteStructure in type(data).__bases__ or Data is type(data) or XYZ in type(data).__bases__ or SingleRegister in type(data).__bases__
        ) else True

class XYZ(ByteStructure):
    """
    Generic XYZ data structure consisting of 3 integers x, y, and z.

    Not recommended to use in practice, as relevant sensor data classes are derived from this.
    """
    physical_convert = 1

    byte_size = 12
    data_format = 'iii'

    def __init__(self, x=0, y=0, z=0):
        """Initializes the XYZ or XYZ-derived object."""
        self.data = [x, y, z]

    def load(self, data, convert=True):
        self.data = data

    def __str__(self):
        return f'X: {self.x}, Y: {self.y}, Z: {self.z}'

    @property
    def x(self):
        return self.data[0] / self.physical_convert

    @x.setter
    def x(self, value):
        self.data[0] = value * self.physical_convert

    @property
    def y(self):
        return self.data[1] / self.physical_convert

    @y.setter
    def y(self, value):
        self.data[1] = value * self.physical_convert

    @property
    def z(self):
        return self.data[2] / self.physical_convert

    @z.setter
    def z(self, value):
        self.data[2] = value * self.physical_convert

    # TODO maybe use asdict()? Move to dataclasses?
    def to_dict(self):
        return {
            "x": self.x,
            "y": self.y,
            "z": self.z,
        }

class Data(ByteStructure):
    """Data allows the user to define arbitrary data structures to use with Pozyx.

    The Leatherman of ByteStructure-derived classes, Data allows you to create your own
    library-compatible packed data structures. Also for empty data, this is used.

    The use of Data:
    Data creates a packed data structure with size and format that is entirely the user's choice.
    The format follows the one used in struct, where b is a byte, h is a 2-byte int, and
    i is a default-sized integer, and f is a float. In capitals, these are unsigned.
    So, to create a custom construct consisting of 4 uint16 and a single int, the
    following code can be used.

      >>> d = Data([0] * 5, 'HHHHi')

    or

      >>> data_format = 'HHHHi'
      >>> d = Data([0] * len(data_format), data_format)

    Args:
        data (optional): Data contained in the data structure. When no data_format is passed, these are assumed UInt8 values.
        data_format (optional): Custom data format for the data passed.
    """

    def __init__(self, data=None, data_format=None):
        if data is None:
            data = []
        self.data = data
        self.data_format = 'B' * len(data) if data_format is None else data_format
        self.set_packed_size()
        self.byte_data = '00' * self.byte_size

    def load(self, data, convert=True):
        self.data = data

class SingleRegister(Data):
    """ SingleRegister is container for the data from a single Pozyx register.

    By default, this represents a UInt8 register. Used for both reading and writing.
    The size and whether the data is a 'signed' integer are both changeable by the
    user using the size and signed keyword arguments.

    Args:
        value (optional): Value of the register.
        size (optional): Size of the register. 1, 2, or 4. Default 1.
        signed (optional): Whether the data is signed. unsigned by default.
        print_hex (optional): How to print the register output. Hex by default. Special options are 'hex' and 'bin'
            other things, such as 'dec', will return decimal output.
    """
    byte_size = 1
    data_format = 'B'

    def __init__(self, value=0, size=1, signed=False, print_style='hex'):
        self.print_style = print_style
        data_format = {1: 'B', 2: 'H', 4: 'I'}.get(size, '')
        if not data_format:
            raise ValueError("Size should be 1, 2, or 4")
        if not signed:
            data_format = data_format.capitalize()
        Data.__init__(self, [value], data_format)

    def load(self, data, convert=True):
        self.data = data

    @property
    def value(self):
        return self.data[0]

    @value.setter
    def value(self, new_value):
        self.data[0] = new_value

    def __str__(self):
        return hex(self.value).capitalize() if (self.print_style == 'hex') else bin(self.value) if (self.print_style == 'bin') else str(self.value)

    def __eq__(self, other):
        if isinstance(other, (SingleRegister, int)):
            return self.value < getattr(other, 'value', other)
        raise ValueError("Can't compare SingleRegister value with non-integer values or registers")

    def __le__(self, other):
        if isinstance(other, (SingleRegister, int)):
            return self.value <= getattr(other, 'value', other)
        raise ValueError("Can't compare SingleRegister value with non-integer values or registers")

    def __lt__(self, other):
        if isinstance(other, (SingleRegister, int)):
            return self.value < getattr(other, 'value', other)
        raise ValueError("Can't compare SingleRegister value with non-integer values or registers")


    def __gt__(self, other):
        return not self.__le__(other)

    def __ge__(self, other):
        return not self.__lt__(other)

class SingleSensorValue(ByteStructure):
    """
    Generic Single Sensor Value data structure.

    Not recommended to use in practice, as relevant sensor data classes are derived from this.
    """
    physical_convert = 1

    byte_size = 4
    data_format = 'i'

    def __init__(self, value=0):
        """Initializes the XYZ or XYZ-derived object."""
        self.data = [0]

        self.load([value])

    @property
    def value(self):
        return self.data[0]

    @value.setter
    def value(self, new_value):
        self.data[0] = new_value

    def load(self, data=None, convert=True):
        self.data = [0] if data is None else data

        if convert:
            self.data[0] = float(self.data[0]) / self.physical_convert

    def __str__(self):
        return f'Value: {self.value}'

def list_serial_ports():
    """Prints the open serial ports line per line"""
    warn("list_serial_ports now deprecated, use print_all_serial_ports instead", DeprecationWarning)
    print(*comports(), sep='\n')


def print_all_serial_ports():
    """Prints the open serial ports line per line"""
    print(*comports(), sep='\n')

def get_serial_ports():
    """Returns the open serial ports"""
    return comports()

def is_pozyx_port(port):
    """Returns whether the port is a Pozyx device"""
    try:
        if "Pozyx Labs" in port.manufacturer:
            return True
    except TypeError:
        pass
    try:
        if "Pozyx" in port.product:
            return True
    except TypeError:
        pass
    try:
        if "0483:" in port.hwid:
            return True
    except TypeError:
        pass
    return False

def get_port_object(device):
    """Returns the PySerial port object from a given port path"""
    for port in get_serial_ports():
        if port.device == device:
            return port

def is_pozyx(device):
    """Returns whether the device is a recognized Pozyx device"""
    port = get_port_object(device)
    return True if (port is not None and is_pozyx_port(port)) else False

def get_pozyx_ports():
    """Returns the Pozyx serial ports. Windows only. Needs driver installed"""
    return [port.device for port in get_serial_ports() if is_pozyx_port(port)]

def get_first_pozyx_serial_port():
    """Returns the first encountered Pozyx serial port's identifier"""
    return next((port.device for port in get_serial_ports() if is_pozyx_port(port)), None)

       
def get_pozyx_ports_windows():
    """Returns the Pozyx serial ports. Windows only. Needs driver installed"""
    pozyx_ports = [port.device for port in get_serial_ports() if "STMicroelectronics Virtual COM Port" in port.description]


def is_correct_pyserial_version():
    """Returns whether the pyserial version is supported"""
    if float(PYSERIAL_VERSION) > 3.4 : 
        return True
    warn("PySerial out of date, please update to v3.4 if possible", stacklevel=0)
    return False

 
class Device(object):
    def __init__(self, id_):
        self._id = id_
        self._firmware_version = None
    
    @property
    def firmware_version(self):
        return self._firmware_version

    @firmware_version.setter
    def firmware_version(self, new_firmware_version):
        self._firmware_version = new_firmware_version[0] if dataCheck(new_firmware_version) else  new_firmware_version

    def has_firmware_version(self):
        return self._firmware_version is not None

    def has_cloud_firmware(self):
        if not self.has_firmware_version():
            print("Device 0x%0.4x has no firmware version" % (self._id))

        return self._firmware_version > 0x11

    def __str__(self):
        return "local device" if self._id is None else "device with ID 0x%0.4x" % self._id

class PozyxCore(object):
    """Implements virtual core Pozyx interfacing functions such as regRead,
    regWrite and regFunction, which have to be implemented in the derived interface.
    Auxiliary functions for core functionality, getRead, setWrite, useFunction,
    and checkForFlag, are also included in PozyxCore.

    waitForFlag_safe, which uses polling, is also implemented interface-independently.

    Apart from these core interface functions, core inter-Pozyx communication
    functionality is also implemented here. This includes remote interface functions
    and general communication to send and receive data from other devices.
    """

    def __init__(self):
        """Constructor for PozyxCore.

        PozyxCore isn't practically usable on its own as it misses a core interface
        implementation.
        """
        pass

    def regRead(self, address, data):
        """Stores the read amount of bytes equal to data's size starting at address into data.

        This is a virtual function, be sure to implement this in your own interface.
        """
        raise NotImplementedError(
            'You need to override this function in your derived interface!')

    def regWrite(self, address, data):
        """Writes the given data starting at address.

        This is a virtual function, be sure to implement this in your own interface.
        """
        raise NotImplementedError(
            'You need to override this function in your derived interface!')

    def regFunction(self, address, params, data):
        """Performs a function with given parameters, storing its output in data.

        This is a virtual function, be sure to implement this in your own interface.
        """
        raise NotImplementedError(
            'You need to override this function in your derived interface!')

    def remoteRegWrite(self, destination, address, data):
        """Performs regWrite on a remote Pozyx device.

        Args:
            destination: Network ID of destination device. integer ID or NetworkID(ID).
            address: Register address to start the writing operation on.
            data: Contains the data to be written. ByteStructure-derived object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if len(data) > PozyxConstants.MAX_BUF_SIZE - 1:
            return POZYX_FAILURE

        send_data = Data([0, address] + data.data, 'BB' + data.data_format)
        status = self.regFunction(PozyxRegisters.WRITE_TX_DATA, send_data, Data([]))
        if status != POZYX_SUCCESS:
            return status

        self.getInterruptStatus(SingleRegister())
        status = self.sendTXWrite(destination)
        if status != POZYX_SUCCESS:
            return status
        return self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, 0.5)

    def remoteRegRead(self, destination, address, data):
        """Performs regRead on a remote Pozyx device.

        Args:
            destination: Network ID of destination device. integer ID or NetworkID(ID).
            address: Register address to start the read operation from.
            data: Container for the read data. ByteStructure-derived object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """

        destination = destination[0] if dataCheck(destination) else destination
        if len(data) > PozyxConstants.MAX_BUF_SIZE or destination == 0:
            return POZYX_FAILURE

        send_data = Data([0, address, data.byte_size])

        status = self.regFunction(PozyxRegisters.WRITE_TX_DATA, send_data, Data([]))
        if status != POZYX_SUCCESS:
            return status

        self.getInterruptStatus(SingleRegister())
        status = self.sendTXRead(destination)
        if status != POZYX_SUCCESS:
            return status

        status = self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, 1)
        if status == POZYX_SUCCESS:
            rx_info = RXInfo()
            self.getRxInfo(rx_info)
            if rx_info.remote_id == destination and rx_info.amount_of_bytes == data.byte_size:
                status = self.readRXBufferData(data)
                return status
            else:
                return POZYX_FAILURE
        return status

    def remoteRegFunction(self, destination, address, params, data):
        """Performs regFunction on a remote Pozyx device.

        Args:
            destination: Network ID of destination device. integer ID or NetworkID(ID).
            address: Register address to start the read operation from.
            params: Parameters for the register function. ByteStructure-derived object of uint8s.
            data: Container for the data returned by the register function. ByteStructure-derived object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        send_data = Data([0, address] + params.data, 'BB' + params.data_format)
        status = self.regFunction(PozyxRegisters.WRITE_TX_DATA, send_data, Data([]))
        if status != POZYX_SUCCESS:
            return status

        self.getInterruptStatus(SingleRegister())
        status = self.sendTXFunction(destination)
        if status != POZYX_SUCCESS:
            return status

        status = self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, 1)
        if status == POZYX_SUCCESS:
            rx_info = RXInfo()
            self.getRxInfo(rx_info)
            if rx_info.remote_id == destination and rx_info.amount_of_bytes == data.byte_size + 1:
                return_data = Data([0] + data.data, 'B' + data.data_format)
                status = self.readRXBufferData(return_data)
                if status != POZYX_SUCCESS:
                    return status
                if len(return_data) > 1:
                    data.load(return_data[1:])
                return return_data[0]
            else:
                return POZYX_FAILURE
        return status

    def waitForFlag(self, interrupt_flag, timeout_s, interrupt=None):
        """Checks the interrupt register for given flag until encountered/past the timeout time.

        This is a virtual function, be sure to implement this in your derived interface.
        """
        raise NotImplementedError(
            'You need to override this function in your derived interface!')

    def waitForFlag_safe(self, interrupt_flag, timeout_s, interrupt=None):
        """Performs waitForFlag in polling mode.

        Args:
            interrupt_flag: Flag of interrupt type to check the interrupt register against.
            timeout_s: duration to wait for the interrupt in seconds.
            interrupt (optional): Container for the interrupt status register data.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not self.suppress_warnings:
            warn("waitForFlag_safe is deprecated, use waitForFlagSafe instead")
        return self.waitForFlagSafe(interrupt_flag, timeout_s, interrupt)

    def clearInterruptStatus(self):
        interrupt = SingleRegister()
        return self.getInterruptStatus(interrupt)

    def waitForFlagSafe(self, interrupt_flag, timeout_s, interrupt=None):
        """Performs waitForFlag in polling mode.

        Args:
            interrupt_flag: Flag of interrupt type to check the interrupt register against.
            timeout_s: duration to wait for the interrupt in seconds.
            interrupt (optional): Container for the interrupt status register data.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        interrupt = SingleRegister() if interrupt is None else interrupt
        start = time()
        while (time() - start) < timeout_s:
            status = self.getInterruptStatus(interrupt)
            if (interrupt[0] & interrupt_flag) and status == POZYX_SUCCESS:
                return True
            sleep(PozyxConstants.DELAY_POLLING_FAST)
        return False

    ## \addtogroup core
    # @{

    def setWrite(self, address, data, remote_id=None, local_delay=PozyxConstants.DELAY_LOCAL_WRITE,
                 remote_delay=PozyxConstants.DELAY_REMOTE_WRITE):
        """Writes data to Pozyx registers either locally or remotely.

        Args:
            address: The register address
            data: A ByteStructure - derived object that contains the data to be written.
            remote_id (optional): Remote ID for remote read.
            local_delay (optional): Delay after a local write
            remote_delay (optional): Delay after a remote write

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        Examples:
            >>> leds = SingleRegister(0xFF)
            >>> self.setWrite(PozyxRegisters.LED_CONTROL, leds)
        """
        if not (is_reg_writable(address)and self.suppress_warnings):
                warn("Register 0x%0.02x isn't writable" % address, stacklevel=3)
        if remote_id is None:
            status = self.regWrite(address, data)
            sleep(local_delay)
        else:
            status = self.remoteRegWrite(remote_id, address, data)
            sleep(remote_delay)
        return status

    def getRead(self, address, data, remote_id=None):
        """Reads Pozyx register data either locally or remotely.

        Args:
            address: The register address
            data: A ByteStructure - derived object that is the container of the read data.

        Kwargs:
            remote_id: Remote ID for remote read.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        Example:
            >>> who_am_i = SingleRegister()
            >>> self.getRead(PozyxRegisters.WHO_AM_I, who_am_i)
            >>> print(who_am_i)
            67
        """
        if not (is_reg_readable(address)and self.suppress_warnings):
                warn("Register 0x%0.02x isn't readable" % address, stacklevel=3)
        return self.regRead(address, data) if (remote_id is None) else self.remoteRegRead(remote_id, address, data)

    def useFunction(self, function, params=None, data=None, remote_id=None):
        """Activates a Pozyx register function either locally or remotely.

        Args:
            address: The function address

        Kwargs:
            params: A ByteStructure - derived object that contains the parameters for the function.
            data: A ByteStructure - derived object that is the container of the read data.
            remote_id: Remote ID for remote read.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        Example:
            >>> self.useFunction(PozyxRegisters.CLEAR_DEVICES)
        """
        if not (is_functioncall(function) and self.suppress_warnings):
                warn("Register 0x%0.02x isn't a function register" % function, stacklevel=3)
        params = Data([]) if params is None else params
        data = Data([]) if data is None else data
        return self.regFunction(function, params, data) if remote_id is None else self.remoteRegFunction(remote_id, function, params, data)
            

    # wait for flag functions

    def checkForFlag(self, interrupt_flag, timeout_s, interrupt=None):
        """Performs waitForFlag_safe and checks against errors or timeouts.

        This abstracts the waitForFlag status check routine commonly encountered
        in more complex library functions and checks the given flag against
        the error flag.

        Args:
            interrupt_flag: Flag of interrupt type to check the interrupt register against.
            timeout_s: duration to wait for the interrupt in seconds
            interrupt (optional): Container for the interrupt status register data.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        interrupt = SingleRegister() if interrupt is None else interrupt
        error_interrupt_mask = PozyxBitmasks.INT_MASK_ERR
        
        if self.waitForFlagSafe(interrupt_flag | error_interrupt_mask, timeout_s, interrupt):
            if (interrupt[0] & error_interrupt_mask) == error_interrupt_mask:
                return POZYX_FAILURE
            else:
                return POZYX_SUCCESS
        else:
            return POZYX_TIMEOUT

    def readRXBufferData(self, data, offset=0):
        """Reads the device's receive buffer's data completely.

        Args:
            data: Container for the data to be read from the receiver buffer.
            offset (optional): Offset of where in the RX buffer to start read

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if data.byte_size + offset > PozyxConstants.MAX_BUF_SIZE:
            return POZYX_FAILURE

        _MAX_SERIAL_SIZE = PozyxConstants.MAX_SERIAL_SIZE

        status = POZYX_SUCCESS
        runs = int(data.byte_size / _MAX_SERIAL_SIZE)
        total_byte_data = ""
        for i in range(runs):
            partial_data = Data([0] * _MAX_SERIAL_SIZE)
            status &= self.regFunction(PozyxRegisters.READ_RX_DATA, Data([offset + i * _MAX_SERIAL_SIZE,
                                                            partial_data.byte_size]), partial_data)
            total_byte_data += partial_data.byte_data
        partial_data = Data([0] * (data.byte_size - runs * _MAX_SERIAL_SIZE))
        status &= self.regFunction(PozyxRegisters.READ_RX_DATA, Data([offset + runs * _MAX_SERIAL_SIZE,
                                                        partial_data.byte_size]), partial_data)
        total_byte_data += partial_data.byte_data
        data.load_bytes(total_byte_data)
        return status

    def writeTXBufferData(self, data, offset=0):
        """Writes data to the device's transmit buffer at the offset address.

        Args:
            data: Data to write to the Pozyx buffer. Has to be a ByteStructure derived object.
            offset (optional): Offset in buffer to start writing data

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        data = Data(data) if not dataCheck(data) else data
        if offset + data.byte_size > PozyxConstants.MAX_BUF_SIZE:
            return POZYX_FAILURE

        # have to account for the parameter taking up a byte
        _MAX_SERIAL_SIZE = PozyxConstants.MAX_SERIAL_SIZE - 1

        status = POZYX_SUCCESS
        data = Data(data.transform_to_bytes())
        runs = int(data.byte_size / _MAX_SERIAL_SIZE)
        for i in range(runs):
            status &= self.regFunction(PozyxRegisters.WRITE_TX_DATA, Data([offset + i * _MAX_SERIAL_SIZE]
                                       + data[i * _MAX_SERIAL_SIZE: (i + 1) * _MAX_SERIAL_SIZE]), Data([]))
        return status & self.regFunction(PozyxRegisters.WRITE_TX_DATA, Data([offset + runs * _MAX_SERIAL_SIZE]
                                                             + data[runs * _MAX_SERIAL_SIZE:]), Data([]))

    def sendTXBufferData(self, destination):
        """Sends the transmit buffer's data to the destination device.

        Args:
            destination: Network ID of destination. integer ID or NetworkID(ID)

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.sendTX(destination, PozyxConstants.REMOTE_DATA)

    def sendTXRead(self, destination):
        """Sends the read operation's data in the transmit buffer to the destination device.

        Args:
            destination: Network ID of destination. integer ID or NetworkID(ID)

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.sendTX(destination, PozyxConstants.REMOTE_READ)

    def sendTXWrite(self, destination):
        """Sends the write operation's data in the transmit buffer to the destination device.

        Args:
            destination: Network ID of destination. integer ID or NetworkID(ID)

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.sendTX(destination, PozyxConstants.REMOTE_WRITE)

    def sendTXFunction(self, destination):
        """Sends the function parameters in the transmit buffer to the destination device.

        Args:
            destination: Network ID of destination. integer ID or NetworkID(ID)

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.sendTX(destination, PozyxConstants.REMOTE_FUNCTION)

    def sendTX(self, destination, operation):
        """Sends the data in the transmit buffer to destination ID. Helper for sendData.

        Args:
            destination: Network ID of destination. integer ID or NetworkID(ID)
            operation: Type of TX operation. These vary depending on the desired operation.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.regFunction(
            PozyxRegisters.SEND_TX_DATA, TXInfo((destination[0] if dataCheck(destination) else destination), operation), Data([])
            )

    def sendData(self, destination, data):
        """Stores the data in the transmit buffer and then sends it to the device with ID destination.

        Args:
            destination: Network ID of destination. integer ID or NetworkID(ID)
            data: Data to send to the destination. Has to be a ByteStructure derived object.

        Performs the following code::

          >>>self.writeTXBufferData(data)
          >>>self.sendTXBufferData(destination)

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        status = POZYX_SUCCESS
        status &= self.writeTXBufferData(data)
        status &= self.sendTXBufferData(destination)
        return status

    ## @}

    def getInterruptStatus(self, interrupts, remote_id=None):
        """Obtains the Pozyx's interrupt register.

        Args:
            interrupts: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.INTERRUPT_STATUS, interrupts, remote_id)

    def getRxInfo(self, rx_info, remote_id=None):
        """Obtain's information on the data the Pozyx received over UWB

        Args:
            rx_info: Container for the RX Info, Data or RXInfo
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.RX_NETWORK_ID, rx_info, remote_id)

class PozyxException(IOError):
    """Base class for Pozyx related exceptions"""
    pass

class PozyxConnectionError(PozyxException):
    """Bad connection to Pozyx gives an exception"""
    pass

class PozyxLib(PozyxCore):
    """Implements the functionality users expect from Pozyx, using the methods from PozyxCore
    to communicate and interface with Pozyx both locally and remotely.
    This does not limit itself to positioning, ranging, and reading the sensor data of
    the various Pozyx sensors, but also features an assortment of troubleshooting functions,
    abstractions of frequently used registers, UWB settings, etc.

    Unlike the Arduino library, this isn't divided into parts such as 'device functions',
    'system functions', etc, but will be in the future. For now, the Arduino library should
    work as a great reference.
    """

    def __init__(self):
        super(PozyxLib, self).__init__()

        self._device_mesh = dict()

    def addIdToDeviceMesh(self, id_=None):
        if id_ in self._device_mesh and self._device_mesh[id_].has_firmware_version():
                return
        else:
            self._device_mesh[id_] = Device(id_)

        device = self._device_mesh[id_]
        firmware_version = SingleRegister()
        status = self.getFirmwareVersion(firmware_version, id_)
        if status == POZYX_SUCCESS:
            device.firmware_version = firmware_version
        else:
            warn(f"Could not obtain firmware version for {device}")
        return status

    # \addtogroup system_functions
    # @{

    def setSensorMode(self, sensor_mode, remote_id=None):
        """Set the Pozyx's sensor mode.

        Args:
            sensor_mode: New sensor mode. See PozyxRegisters.SENSORS_MODE register. integer sensor_mode or SingleRegister(sensor_mode).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(sensor_mode):
            sensor_mode = SingleRegister(sensor_mode)
        if sensor_mode[0] < 0 or sensor_mode[0] > 12:
            warn(f"setSensorMode: mode {sensor_mode} not valid")
        status = self.setWrite(PozyxRegisters.SENSORS_MODE, sensor_mode, remote_id)
        # legacy delay?
        sleep(PozyxConstants.DELAY_MODE_CHANGE)
        return status

    def resetSystem(self, remote_id=None):
        """Resets the Pozyx device.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.useFunction(PozyxRegisters.RESET_SYSTEM, remote_id=remote_id)

    def saveRegisters(self, registers, remote_id=None):
        """Saves the given registers to the Pozyx's flash memory, if these are writable registers.

        This means that upon reset, the Pozyx will use these saved values instead of the default values.
        This is especially practical when changing UWB settings of an entire network, making it unnecessary
        to re - set these when resetting or repowering a device.\n
        DISCLAIMER: Make sure to not abuse this function in your code, as the flash memory only has a finite
        number of writecycles available, adhere to the Arduino's mentality in using flash memory.

        Args:
            registers: Registers to save to the flash memory. Data([register1, register2, ...]) or [register1, register2, ...]
                These registers have to be writable. Saving the UWB gain is currently not working.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.saveConfiguration(
            PozyxConstants.FLASH_SAVE_REGISTERS, registers, remote_id
            )

    def getNumRegistersSaved(self, remote_id=None):
        """Obtains the number of registers saved to the Pozyx's flash memory.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            The number of saved registers.
        """
        details = Data([0] * 20)
        return POZYX_FAILURE if (
            self.useFunction(PozyxRegisters.GET_FLASH_DETAILS, data=details, remote_id=remote_id) == POZYX_FAILURE
            ) else sum(bin(details[i]).count('1') for i in range(20))


    def isRegisterSaved(self, register_address, remote_id=None):
        """Returns whether the given register is saved to the Pozyx's flash memory.

        Args:
            register_address: Register address to check if saved
            remote_id (optional): Remote Pozyx ID.

        Returns:
            1 if the register is saved, 0 if it's not.
        """
        details = Data([0] * 20)
        if self.useFunction(PozyxRegisters.GET_FLASH_DETAILS, data=details, remote_id=remote_id) == POZYX_FAILURE:
            return POZYX_FAILURE
        byte_num = int(register_address / 8)
        bit_num = register_address % 8
        return (details[byte_num] >> bit_num) & 0x1

    def getSavedRegisters(self, remote_id=None):
        details = Data([0] * 20)
        return POZYX_FAILURE if (self.useFunction(PozyxRegisters.GET_FLASH_DETAILS, data=details, remote_id=remote_id) == POZYX_FAILURE) else details

    def setConfigGPIO(self, gpio_num, mode, pull, remote_id=None):
        """Set the Pozyx's selected GPIO pin configuration(mode and pull).

        Args:
            gpio_num: GPIO pin number, 1 to 4.
            mode: GPIO configuration mode. integer mode or SingleRegister(mode)
            pull: GPIO configuration pull. integer pull or SingleRegister(pull)
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(mode):
            mode = SingleRegister(mode)
        if not dataCheck(pull):
            pull = SingleRegister(pull)

        if not 1 <= gpio_num <= 4:
            warn(f"setConfigGPIO: GPIO number {gpio_num} not in range")
        if mode[0] in PozyxConstants.ALL_GPIO_MODES:
            warn(f"setConfigGPIO: {mode[0]} wrong GPIO mode")
        if pull[0] in PozyxConstants.ALL_GPIO_PULLS:
            warn(f"setConfigGPIO: {pull[0]} wrong GPIO pull")

        gpio_register = PozyxRegisters.CONFIG_GPIO_1 + gpio_num - 1
        mask = Data([mode[0] + (pull[0] << 3)])
        return self.setWrite(gpio_register, mask, remote_id)

    def setGPIO(self, gpio_num, value, remote_id=None):
        """Set the Pozyx's selected GPIO pin output.

        Args:
            gpio_num: GPIO pin number, 1 to 4
            value: GPIO output value, either HIGH(1) or LOW(0). Physically, 3.3V or 0V. integer value or SingleRegister(value).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(value):
            value = SingleRegister(value)

        if not 1 <= gpio_num <= 4:
            warn(f"setGPIO: GPIO number {gpio_num} not in range")
        if not (value[0] == 0 or value[0] == 1):
            warn(f"setGPIO: wrong value {value[0]}, please choose integer 1 (HIGH) or 0 (LOW)")

        gpio_register = PozyxRegisters.GPIO_1 + gpio_num - 1
        return self.setWrite(gpio_register, value, remote_id)

    def setLed(self, led_num, state, remote_id=None):
        """Set the Pozyx's selected LED state.

        Args:
            led_num: LED pin number, 1 to 4
            state: LED output state. Boolean. True = on and False = off, you can use POZYX_LED_ON and POZYX_LED_OFF instead.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not 1 <= led_num <= 4:
            warn(f"setLed: LED number {led_num} not in range")
        if not (isinstance(state,bool)):
            warn(f"setLed: wrong state {state}, please choose boolean True or False")

        params = Data([0x1 << (led_num - 1 + 4) |
                       ((state << led_num - 1) % 256)])
        return self.useFunction(PozyxRegisters.LED_CONTROL, params, None, remote_id)

    def clearConfiguration(self, remote_id=None):
        """Clears the Pozyx's flash memory.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        self.getInterruptStatus(SingleRegister())
        status = self.useFunction(
            PozyxRegisters.RESET_FLASH_MEMORY, remote_id=remote_id)
        if status == POZYX_FAILURE:
            warn("Error clearing the flash memory")
            return status
        # give the device some time to clear the flash memory
        sleep(PozyxConstants.DELAY_FLASH)
        return status

    def configInterruptPin(self, pin_number=0, mode=0, active_high=False, latch=False, remote_id=None):
        """Configures the interrupt pin via the PozyxRegisters.INTERRUPT_PIN register.

        Args:
            pin_number (optional): The Pozyx's pin ID. 1 to 4 on anchor, 1 to 6 on tag. 0 means no pin. SingleRegister or integer.
            mode (optional): Push-pull (0) or pull (1). SingleRegister or integer. SingleRegister or integer.
            active_high (optional): Is the interrupt voltage active high or low. Boolean.
            latch (optional): Is the interrupt a short pulse or latch till read? Boolean.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if dataCheck(pin_number):
            pin_number = pin_number[0]
        if dataCheck(mode):
            mode = mode[0]
        if not 0 <= pin_number <= 6:
            warn(f'Error: Pin number {pin_number} is invalid, should be between 0 and 6')
        if not mode == 0 or mode == 1:
            warn(f"Error: Mode {mode} is invalid, should be 0 (PUSH-PULL) or 1 (PULL).")
        int_config = SingleRegister(pin_number + (mode << 3) + (active_high << 4) + (latch << 5))
        self.setWrite(PozyxRegisters.INTERRUPT_PIN, int_config, remote_id)

    def getWhoAmI(self, who_am_i, remote_id=None):
        """Obtains the Pozyx's WHO_AM_I.

        Args:
            who_am_i: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.WHO_AM_I, who_am_i, remote_id)

    def getFirmwareVersion(self, firmware, remote_id=None):
        """Obtains the Pozyx's firmware version.

        Args:
            firmware: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.FIRMWARE_VERSION, firmware, remote_id)

    def getHardwareVersion(self, hardware, remote_id=None):
        """Obtains the Pozyx's hardware version.

        Args:
            hardware: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.HARDWARE_VERSION, hardware, remote_id)

    def getSelftest(self, selftest, remote_id=None):
        """Obtains the Pozyx's selftest.

        Args:
            selftest: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.SELFTEST_RESULT, selftest, remote_id)

    def getErrorCode(self, error_code, remote_id=None):
        """Obtains the Pozyx's error code.

        Args:
            error_code: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.ERROR_CODE, error_code, remote_id)

    def getCalibrationStatus(self, calibration_status, remote_id=None):
        """Obtains the Pozyx's calibration status.

        Args:
            calibration_status: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.CALIBRATION_STATUS, calibration_status, remote_id)

    def getDeviceDetails(self, system_details, remote_id=None):
        """

        Args:
            system_details: Container for the read data. DeviceDetails.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getWhoAmI(system_details, remote_id=remote_id)
        if system_details.id is None:
            if remote_id is None:
                network_id = NetworkID()
                status &= self.getNetworkId(network_id)
                system_details.id = network_id.id
            else:
                system_details.id = remote_id
        return status


    def getInterruptMask(self, mask, remote_id=None):
        """Obtains the Pozyx's interrupt mask.

        Args:
            mask: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.INTERRUPT_MASK, mask, remote_id)

    def getConfigModeGPIO(self, gpio_num, mode, remote_id=None):
        """Obtain the Pozyx's configuration mode of the selected GPIO pin.

        Args:
            gpio_num: GPIO pin number, 1 to 4.
            mode: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getGPIO, getConfigPullGPIO
        """
        if not 1 <= gpio_num <= 4:
            warn(f"getConfigModeGPIO: GPIO number {gpio_num} not in range")
        gpio_register = PozyxRegisters.CONFIG_GPIO_1 + gpio_num - 1
        status = self.getRead(gpio_register, mode, remote_id)
        mode[0] &= 0x7
        return status

    def getConfigPullGPIO(self, gpio_num, pull, remote_id=None):
        """Obtain the Pozyx's selected GPIO pin pull.

        Args:
            gpio_num: GPIO pin number, 1 to 4.
            pull: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getGPIO, getConfigModeGPIO
        """
        if not 1 <= gpio_num <= 4:
            warn(f"getConfigPullGPIO: GPIO number {gpio_num} not in range")
        gpio_register = PozyxRegisters.CONFIG_GPIO_1 + gpio_num - 1
        status = self.getRead(gpio_register, pull, remote_id)
        pull[0] = (pull[0] & 0x18) >> 3
        return status

    def getGPIO(self, gpio_num, value, remote_id=None):
        """Obtain the Pozyx's value of the selected GPIO pin, being either HIGH or LOW(physically 3.3V or 0V).

        Args:
            gpio_num: GPIO pin number, 1 to 4.
            value: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getConfigPullGPIO, getConfigModeGPIO
        """
        if not 1 <= gpio_num <= 4:
            warn(f"getGPIO: GPIO number {gpio_num} not in range")
        gpio_register = PozyxRegisters.GPIO_1 + gpio_num - 1
        return self.getRead(gpio_register, value, remote_id)

    def getErrorMessage(self, error_code):
        """Returns the system error string for the given error code

        Args:
            error_code: Error code for which to return the error message. int or SingleRegister

        Returns:
            string with error description

        See Also:
            getErrorCode, getSystemError
        """
        if dataCheck(error_code):
            error_code = error_code.value
        return ERROR_MESSAGES.get(error_code, "Unknown error 0x%0.02x" % error_code)

    def getSystemError(self, remote_id=None):
        """Returns the Pozyx's system error string.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            string with error description

        See Also:
            getErrorCode, getErrorMessage
        """
        error_code = SingleRegister()
        status = self.getErrorCode(error_code, remote_id)

        if status == POZYX_SUCCESS:
            return self.getErrorMessage(error_code)
        return "Error: couldn't retrieve error from device."

    def setInterruptMask(self, mask, remote_id=None):
        """Set the Pozyx's interrupt mask.

        Args:
            mask: Interrupt mask. See PozyxRegisters.INTERRUPT_MASK register. integer mask or SingleRegister(mask)
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(mask):
            mask = SingleRegister(mask)
        return self.setWrite(PozyxRegisters.INTERRUPT_MASK, mask, remote_id)

    def setLedConfig(self, config, remote_id=None):
        """Set the Pozyx's LED configuration.

        Args:
            config: LED configuration. See PozyxRegisters.LED_CONFIGURATION register. integer configuration or SingleRegister(configuration)
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(config):
            config = SingleRegister(config)
        return self.setWrite(PozyxRegisters.LED_CONFIGURATION, config, remote_id)

    def saveAnchorIds(self, remote_id=None):
        """Saves the anchor IDs used in positioning to the Pozyx's flash memory.

        This means that upon reset, the Pozyx won't need to be recalibrated before performing positioning.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        self.saveConfiguration(PozyxConstants.FLASH_SAVE_ANCHOR_IDS, remote_id=remote_id)

    def getUpdateInterval(self, ms, remote_id=None):
        """Obtains the Pozyx's update interval.

        Args:
            ms: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.POSITIONING_INTERVAL, ms, remote_id)

    def getRangingProtocol(self, protocol, remote_id=None):
        """Obtains the Pozyx's ranging protocol

        Args:
            protocol: Container for the read protocol data. SingleRegister or Data([0])
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.RANGING_PROTOCOL, protocol, remote_id)

    def setRangingProtocolFast(self, remote_id=None):
        return self.setRangingProtocol(PozyxConstants.RANGE_PROTOCOL_FAST, remote_id=remote_id)

    def setRangingProtocolPrecision(self, remote_id=None):
        return self.setRangingProtocol(PozyxConstants.RANGE_PROTOCOL_PRECISION, remote_id=remote_id)

    def setRangingProtocol(self, protocol, remote_id=None):
        """Set the Pozyx's ranging protocol.

        Args:
            protocol: the new ranging protocol. See PozyxRegisters.RANGING_PROTOCOL register. integer or SingleRegister(protocol)
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(protocol):
            protocol = SingleRegister(protocol)
        if not 0 <= protocol[0] <= 1:
            warn(f"setRangingProtocol: wrong protocol {protocol[0]}")

        return self.setWrite(PozyxRegisters.RANGING_PROTOCOL, protocol, remote_id)

    def getPositioningAlgorithmData(self, algorithm_data, remote_id=None):
        """Obtains the Pozyx's positioning algorithm.

        Args:
            algorithm_data: Container for the read data. AlgorithmData or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.POSITIONING_ALGORITHM, algorithm_data, remote_id)

    def setPositioningAlgorithmData(self, algorithm_data, remote_id=None):
        """Obtains the Pozyx's positioning algorithm.

        Args:
            algorithm_data: Container for the read data. AlgorithmData or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.setWrite(PozyxRegisters.POSITIONING_ALGORITHM. algorithm_data, remote_id=remote_id)

    def getPositionAlgorithm(self, algorithm, remote_id=None):
        """Obtains the Pozyx's positioning algorithm.

        Args:
            algorithm: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getRead(PozyxRegisters.POSITIONING_ALGORITHM, algorithm, remote_id)
        algorithm[0] &= 0xF
        return status

    def getPositionDimension(self, dimension, remote_id=None):
        """Obtains the Pozyx's positioning dimension.

        Args:
            dimension: Container the for read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getRead(PozyxRegisters.POSITIONING_ALGORITHM, dimension, remote_id)
        dimension[0] = (dimension[0] & 0x30) >> 4
        return status

    def getAnchorSelectionMode(self, mode, remote_id=None):
        """Obtains the Pozyx's anchor selection mode.

        Args:
            mode: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getRead(PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS, mode, remote_id)
        mode[0] = (mode[0] & 0x80) >> 7
        return status

    def getNumberOfAnchors(self, nr_anchors, remote_id=None):
        """Obtains the Pozyx's number of selected anchors.

        Args:
            nr_anchors: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getRead(PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS, nr_anchors, remote_id)
        nr_anchors[0] &= 0xF
        return status

    def getOperationMode(self, mode, remote_id=None):
        """Obtains the Pozyx's mode of operation.

        Args:
            mode: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.OPERATION_MODE, mode, remote_id)

    def getCoordinates(self, coordinates, remote_id=None):
        """Obtains the Pozyx's coordinates. These are either set manually or by positioning.

        Args:
            coordinates: Container for the read data. Coordinates().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.POSITION_X, coordinates, remote_id)

    def getHeight(self, height, remote_id=None):
        """Obtains the Pozyx's height coordinate.

        Args:
            height: Container for the read height data. Data([0], 'i').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.POSITION_Z, height, remote_id)

    def getPositionError(self, positioning_error, remote_id=None):
        """Obtains the Pozyx's positioning error.

        Args:
            positioning_error: Container for the read data. PositionError().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.POSITIONING_ERROR_X, positioning_error, remote_id)

    def getPositioningAnchorIds(self, anchors, remote_id=None):
        """Obtain the IDs of the anchors in the Pozyx's device list used for positioning.

        You need to make sure to know how many anchors are used, as an incorrect
        size of anchors will cause the function to fail. Use getNumberOfAnchors
        to know this number.

        Args:
            anchors: Container for the read data. DeviceList(list_size=size)
            or Data([0] * size, 'H' * size).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getAnchorIds, getTagIds, getDeviceIds

        Example:
            >> > list_size = SingleRegister()
            >> > self.getNumberOfAnchors(list_size)
            >> > anchor_list = DeviceList(list_size=list_size[0])
            >> > self.getPositioningAnchorIds(anchor_list)
            >> > print(anchor_list)
            '0x6720, 0x6811, 0x6891'
        """
        # TODO remove this check altogether in 2.0
        if not 0 < len(anchors) <= 16:
            warn("getPositioningAnchorIds: Anchor number out of range, use between 0-10 anchors")
        device_list_size = SingleRegister()
        status = self.getDeviceListSize(device_list_size, remote_id)
        if len(anchors) < device_list_size[0] or status == POZYX_FAILURE:
            return POZYX_FAILURE
        return self.useFunction(PozyxRegisters.GET_POSITIONING_ANCHOR_IDS, Data([]), anchors, remote_id)

    def getDeviceRangeInfo(self, device_id, device_range, remote_id=None):
        """Obtain the range information of the device with selected ID in the Pozyx's device list.

        Args:
            device_id: ID of desired device whose range measurement is of interest. NetworkID()
            or Data([ID], 'H') or integer ID.
            device_range: Container for the read data. DeviceRange().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(device_id):
            device_id = NetworkID(device_id)

        if device_id[0] < 0 or device_id[0] > 0xFFFF:
            warn(f"getDeviceRangeInfo: device ID should be between 0x0000 and 0xFFFF, not {device_id[0]}")

        return self.useFunction(PozyxRegisters.GET_DEVICE_RANGE_INFO, device_id, device_range, remote_id)

    def setUpdateInterval(self, ms, remote_id=None):
        """Set the Pozyx's update interval in ms(milliseconds).

        Args:
            ms: Update interval in ms. integer ms or SingleRegister(ms, size=2)
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(ms):
            ms = SingleRegister(ms, size=2)
        if not 100 < ms[0] <= 60000:
            warn(f"setUpdateInterval: ms not 100 < ms < 60000, is {ms[0]}")
        return self.setWrite(PozyxRegisters.POSITIONING_INTERVAL, ms, remote_id)

    def setCoordinates(self, coordinates, remote_id=None):
        """Set the Pozyx's coordinates.

        Args:
            coordinates: Desired Pozyx coordinates. Coordinates() or [x, y, z].
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(coordinates):
            coordinates = Coordinates(
                coordinates[0], coordinates[1], coordinates[2])
        return self.setWrite(PozyxRegisters.POSITION_X, coordinates, remote_id)

    def setHeight(self, height, remote_id=None):
        """Sets the Pozyx device's height.

        Args:
            height: Desired Pozyx height. integer height or Data([height], 'i').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(height):
            height = Data([height], 'i')
        return self.setWrite(PozyxRegisters.POSITION_Z, height, remote_id=remote_id)

    def setPositioningFilterNone(self, remote_id=None):
        return self.setPositionFilter(PozyxConstants.FILTER_TYPE_NONE, SingleRegister(), remote_id=remote_id)

    def setPositioningFilterFIR(self, filter_strength, remote_id=None):
        return self.setPositionFilter(PozyxConstants.FILTER_TYPE_FIR, filter_strength, remote_id=remote_id)

    def setPositioningFilterMovingMedian(self, filter_strength, remote_id=None):
        return self.setPositionFilter(PozyxConstants.FILTER_TYPE_MOVING_MEDIAN, filter_strength, remote_id=remote_id)

    def setPositioningFilterMovingAverage(self, filter_strength, remote_id=None):
        return self.setPositionFilter(PozyxConstants.FILTER_TYPE_MOVING_AVERAGE, filter_strength, remote_id=remote_id)

    def setPositionFilter(self, filter_type, filter_strength, remote_id=None):
        """Set the Pozyx's positioning filter.

        Note that currently only PozyxConstants.FILTER_TYPE_MOVING_AVERAGE, PozyxConstants.FILTER_TYPE_MOVING_MEDIAN and PozyxConstants.FILTER_TYPE_FIR are implemented.

        Args:
            filter_type: Positioning filter type. Integer or SingleRegister.
            filter_strength: Positioning filter strength. Integer or SingleRegister.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(filter_strength):
            filter_strength = SingleRegister(filter_strength)
        if not dataCheck(filter_type):
            filter_type = SingleRegister(filter_type)

        if not filter_type[0] in PozyxConstants.FILTER_TYPES:
            warn(f"setPositionFilter: invalid filter type {filter_type[0]}")
        if not 0 <= filter_strength[0] <= 15:
            warn(f"setPositionFilter: invalid strength {filter_strength[0]}, keep between 0 and 15")

        params = Data([filter_type[0] + (filter_strength[0] << 4)])
        return self.setWrite(PozyxRegisters.POSITIONING_FILTER, params, remote_id)

    def getPositionFilterData(self, filter_data, remote_id=None):
        """**NEW**! Get the positioning filter data.

        Use FilterData if you want to have a ready to go container for this data.

        Args:
            filter_data: Container for filter data. SingleRegister or FilterData
            remote_id (optional): Remote Pozyx ID.

        Example:
            >>> pozyx = PozyxLib()  # PozyxSerial has PozyxLib's functions, just for generality
            >>> filter_data = FilterData()
            >>> pozyx.getPositionFilter(filter_data)
            >>> print(filter_data)  # "Moving average filter with strength 10"
            >>> print(filter_data.get_filter_name())  # "Moving average filter"
            >>> print(filter_data.algorithm)  # "3"
            >>> print(filter_data.dimension())  # "10"

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.POSITIONING_FILTER, filter_data, remote_id=remote_id)

    def getPositionFilterStrength(self, remote_id=None):
        """**NEW**! Get the positioning filter strength.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        filter_data = FilterData()
        status = self.getPositionFilterData(filter_data, remote_id=remote_id)

        if status != POZYX_SUCCESS:
            warn("Wasn't able to get filter data, returning -1 as strength")
            return -1
        return filter_data.filter_strength

    def setPositionAlgorithmNormal(self, remote_id=None):
        dimension = SingleRegister()
        self.getPositionDimension(dimension, remote_id=remote_id)
        return self.setPositionAlgorithm(PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY, dimension, remote_id=remote_id)

    def setPositionAlgorithmTracking(self, remote_id=None):
        dimension = SingleRegister()
        self.getPositionDimension(dimension, remote_id=remote_id)
        return self.setPositionAlgorithm(PozyxConstants.POSITIONING_ALGORITHM_TRACKING, dimension, remote_id=remote_id)

    def setPositionAlgorithm(self, algorithm, dimension, remote_id=None):
        """Set the Pozyx's positioning algorithm.

        Note that currently only PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY and PozyxConstants.POSITIONING_ALGORITHM_TRACKING are implemented.

        Args:
            algorithm: Positioning algorithm. integer algorithm or SingleRegister(algorithm).
            dimension: Positioning dimension. integer dimension or SingleRegister(dimension).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(algorithm):
            algorithm = SingleRegister(algorithm)
        if not dataCheck(dimension):
            dimension = SingleRegister(dimension)

        if not algorithm[0] in PozyxConstants.POSITIONING_ALGORITHMS:
            warn(f"setPositionAlgorithm: wrong algorithm {algorithm[0]}")
        if not dimension[0] in PozyxConstants.DIMENSIONS:
            warn(f"setPositionAlgorithm: wrong dimension {dimension[0]}")

        params = Data([algorithm[0] + (dimension[0] << 4)])
        return self.setWrite(PozyxRegisters.POSITIONING_ALGORITHM, params, remote_id)

    def setSelectionOfAnchorsAutomatic(self, number_of_anchors, remote_id=None):
        return self.setSelectionOfAnchors(PozyxConstants.ANCHOR_SELECT_AUTO, number_of_anchors, remote_id=remote_id)

    def setSelectionOfAnchorsManual(self, number_of_anchors, remote_id=None):
        return self.setSelectionOfAnchors(PozyxConstants.ANCHOR_SELECT_MANUAL, number_of_anchors, remote_id=remote_id)

    def setSelectionOfAnchors(self, mode, number_of_anchors, remote_id=None):
        """Set the Pozyx's coordinates.

        Args:
            mode: Anchor selection mode. integer mode or SingleRegister(mode).
            number_of_anchors (int, SingleRegister): Number of anchors used in positioning. integer nr_anchors or SingleRegister(nr_anchors).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(mode):
            mode = SingleRegister(mode)
        if not dataCheck(number_of_anchors):
            number_of_anchors = SingleRegister(number_of_anchors)

        if not (mode[0] == PozyxConstants.ANCHOR_SELECT_MANUAL or mode[0] == PozyxConstants.ANCHOR_SELECT_AUTO):
            warn(f"setSelectionOfAnchors: wrong mode {mode[0]}")
        if not 2 < number_of_anchors[0] <= 16:
            warn(f"setSelectionOfAnchors: number of anchors {number_of_anchors[0]} not in range 3-16")

        params = Data([(mode[0] << 7) + number_of_anchors[0]])
        return self.setWrite(PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS, params, remote_id)

    def setPositioningAnchorIds(self, anchors, remote_id=None):
        """Set the anchors the Pozyx will use for positioning.

        Args:
            anchors: List of anchors that'll be used for positioning. DeviceList() or [anchor_id1, anchor_id2, ...]
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(anchors):
            anchors = DeviceList(anchors)
        if not 0 < len(anchors) <= 10:
            warn(f"setPositioningAnchorIds: size {len(anchors)} not in range 1-10")

        return self.useFunction(PozyxRegisters.SET_POSITIONING_ANCHOR_IDS, anchors, None, remote_id)

    def doRanging(self, destination_id, device_range, remote_id=None):
        """Performs ranging with another destination device, resulting in range information.

        This is pretty straightforward, the range information consists of the following:
            - the timestamp of the range measurement.
            - the distance between the local / remote tag and the destination
            - the RSS, which indicates the signal strength between origin and destination.

        While in the Arduino library doRemoteRanging is used for remote ranging, this function
        follows the library's convention to add remote_id as a keyword argument. Make sure that
        the destination is on the same UWB settings as this, and to pass a DeviceRange object
        for the device_range parameter.

        For an in-action example, check the "Ready to range" tutorial on the Pozyx homepage(www.pozyx.io),
        and the ready_to_range.py example found in this library's tutorial folder.

        Args:
            destination_id: Network ID of the destination, to perform ranging with. integer ID or NetworkID(ID)
            device_range: Container for device range measurement data. DeviceRange object.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(destination_id):
            destination_id = NetworkID(destination_id)
        if destination_id[0] < 0 or destination_id[0] > 0xFFFF:
            warn("Destination ID should be between 0x0000 and 0xFFFF, not {}".format(destination_id[0]))

        self.clearInterruptStatus()

        int_flag = PozyxBitmasks.INT_STATUS_FUNC
        if remote_id is not None:
            int_flag = PozyxBitmasks.INT_STATUS_RX_DATA

        status = self.useFunction(
            PozyxRegisters.DO_RANGING, destination_id, Data([]), remote_id=remote_id)
        if status == POZYX_SUCCESS:
            status = self.checkForFlag(int_flag, PozyxConstants.DELAY_INTERRUPT)
            if status == POZYX_SUCCESS:
                self.getDeviceRangeInfo(destination_id, device_range, remote_id=remote_id)
            return status
        return POZYX_FAILURE

    def doRangingSlave(self, destination_id, device_range):
        """Checks whether the device has ranged and if so, reads the range.

        This is useful for slave devices with a controller that needs to know the range measurements too

        Args:
            destination_id: Network ID of the destination, to perform ranging with. integer ID or NetworkID(ID)
            device_range: Container for device range measurement data. DeviceRange object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(destination_id):
            destination_id = NetworkID(destination_id)
        if destination_id[0] < 0 or destination_id[0] > 0xFFFF:
            warn(f"Destination ID should be between 0x0000 and 0xFFFF, not {destination_id[0]}")

        status = self.checkForFlag(PozyxBitmasks.INT_MASK_FUNC, PozyxConstants.DELAY_INTERRUPT)
        if status == POZYX_SUCCESS:
            self.getDeviceRangeInfo(destination_id, device_range)
        return status

    def doPositioning(self, position, dimension=PozyxConstants.DIMENSION_3D, height=Data([0], 'i'), algorithm=None, remote_id=None, timeout=None):
        """Performs positioning with the Pozyx. This is probably why you're using Pozyx.

        This function only performs the positioning and doesn't take care of the previous steps
        required to get this operational, so be sure to adhere to this checklist:
        - while you can perform automatic calibration, manual calibration is much more stable and reliable.
        - when using manual calibration, add all anchors using addDevice.
        - all anchors are on the same UWB settings as the device performing positioning.
        - if you're using more than four anchors, be sure to set this with setSelectionOfAnchors.

        Basic troubleshooting:
        - try to perform ranging with all devices
        - are you using a Coordinates object for your position?
        - if you perform getDeviceListSize and subsequently getDeviceIds, are these your anchors?

        While in the Arduino library doRemotePositioning is used for remote ranging, this function
        follows the library's convention to add remote_id as a keyword argument.

        For an in-action example, check the "Ready to localize" tutorial on the Pozyx homepage(www.pozyx.io),
        and the ready_to_localize.py example found in this library's tutorial folder.

        Args:
            position: Container for the positioning coordinates. Coordinates object.
            dimension (optional): Dimension to perform positioning in. Default 3D. When 2.5D, make sure height is also passed along.
            height (optional): Height of Pozyx in 2.5D positioning. Default 0. Either integer height or Data([height], 'i').
            algorithm (optional): Algorithm set before positioning. No new algorithm is set by default.
            remote_id (optional): Remote Pozyx ID. Local Pozyx is used when None or omitted.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if algorithm not in [PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY, PozyxConstants.POSITIONING_ALGORITHM_TRACKING, None]:
            warn(f"doPositioning: not existing algorithm {algorithm}")
        if dimension not in [PozyxConstants.DIMENSION_3D, PozyxConstants.DIMENSION_2D, PozyxConstants.DIMENSION_2_5D]:
            warn(f"doPositioning: not existing dimension {dimension}")

        if algorithm is not None:
            alg_options = Data([dimension << 4 | algorithm])
            self.setWrite(PozyxRegisters.POSITIONING_ALGORITHM, alg_options, remote_id)
        if dimension == PozyxConstants.DIMENSION_2_5D:
            if not dataCheck(height):
                height = Data([height], 'i')
            self.setWrite(PozyxRegisters.POSITION_Z, height, remote_id)

        if remote_id not in self._device_mesh:
            status = self.addIdToDeviceMesh(remote_id)
            if status != POZYX_SUCCESS:
                del self._device_mesh[remote_id]
                return status
            
        if self._device_mesh[remote_id].has_cloud_firmware():
            if timeout is None:
                timeout = PozyxConstants.TIMEOUT_POSITIONING if remote_id is None else PozyxConstants.TIMEOUT_REMOTE_POSITIONING
            position_data = PositioningData(0b1)
            status = self.doPositioningWithData(position_data, remote_id=remote_id, timeout=timeout)
            if status == POZYX_SUCCESS:
                position.load_bytes(position_data.byte_data)
            return status

        status = self.useFunction(PozyxRegisters.DO_POSITIONING, remote_id=remote_id, params=Data([1], "H"))
        if status != POZYX_SUCCESS:
            return POZYX_FAILURE

        if remote_id is None:
            timeout = PozyxConstants.TIMEOUT_POSITIONING if timeout is None else timeout
            status = self.checkForFlag(PozyxBitmasks.INT_STATUS_POS, timeout)
            if status == POZYX_SUCCESS:
                return self.getCoordinates(position)
            return status
        else:
            timeout = PozyxConstants.TIMEOUT_REMOTE_POSITIONING if timeout is None else timeout
            if self.waitForFlag(PozyxBitmasks.INT_STATUS_RX_DATA, timeout) == POZYX_SUCCESS:
                rx_info = Data([0, 0], 'HB')
                self.getRead(PozyxRegisters.RX_NETWORK_ID, rx_info)
                if rx_info[0] == remote_id and rx_info[1] == position.byte_size:
                    status = self.readRXBufferData(position)
                    # necessary to update x, y, z variables of position.
                    position.load(position.data)
                    return status
                else:
                    return POZYX_FAILURE
        return POZYX_TIMEOUT

    def getPositioningData(self, positioning_data):
        flags = Data([positioning_data.flags], 'H')
        flags.load_hex_string()
        s = 'F,%0.2x,%s,%i\r' % (PozyxRegisters.DO_POSITIONING_WITH_DATA, flags.byte_data, positioning_data.byte_size + 61)
        # very custom solution...
        r = self.serialExchange(s)
        if positioning_data.has_ranges():
            amount_of_ranges = int(r[2 * positioning_data.byte_size:2 * positioning_data.byte_size + 2], 16)
            positioning_data.set_amount_of_ranges(amount_of_ranges)
            r = r[: positioning_data.byte_size * 2 + 2]
        if len(positioning_data) > 0:
            positioning_data.load_bytes(r[2:])
        return int(r[0:2], 16)

    def doPositioningWithData(self, positioning_data, remote_id=None, timeout=None):
        if remote_id is None:
            status = self.useFunction(PozyxRegisters.DO_POSITIONING)
            timeout = PozyxConstants.TIMEOUT_POSITIONING_DATA if timeout is None else timeout
            if status != POZYX_SUCCESS:
                return status
            status = self.checkForFlag(PozyxBitmasks.INT_STATUS_POS, timeout)
            if status == POZYX_SUCCESS:
                return self.getPositioningData(positioning_data)
            return status
        else:
            timeout = PozyxConstants.TIMEOUT_REMOTE_POSITIONING_DATA if timeout is None else timeout
            flags_data = Data([positioning_data.flags], 'H')
            self.remoteRegFunctionWithoutCheck(remote_id, PozyxRegisters.DO_POSITIONING, flags_data)

            status = self.waitForFlag(PozyxBitmasks.INT_STATUS_RX_DATA, timeout)
            if status == POZYX_SUCCESS:
                rx_info = RXInfo()
                self.getRxInfo(rx_info)

                if positioning_data.has_ranges():
                    amount_of_ranges = int(
                        (rx_info.amount_of_bytes - positioning_data.byte_size) / RangeInformation.byte_size)
                    positioning_data.set_amount_of_ranges(amount_of_ranges)

                if rx_info.remote_id == remote_id:
                    status = self.readRXBufferData(positioning_data)
                    return status
                else:
                    return POZYX_FAILURE
            return status


    def doPositioningSlave(self, position, timeout=None):
        """Checks whether the device has positioned and if so, reads the position.

        This is useful for slave devices with a controller that needs to know the device's positions too

        Args:
            position: Container for the positioning coordinates. Coordinates object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        timeout = PozyxConstants.TIMEOUT_POSITIONING if timeout is None else timeout

        if None not in self._device_mesh:
            status = self.addIdToDeviceMesh(None)
            if status != POZYX_SUCCESS:
                del self._device_mesh[None]
                return status

        if self._device_mesh[None].has_cloud_firmware():
            position_data = PositioningData(0b1)
            status = self.doPositioningWithDataSlave(position_data, timeout=timeout)
            if status == POZYX_SUCCESS:
                position.load_bytes(position_data.byte_data)
            return status
        else:
            status = self.checkForFlag(PozyxBitmasks.INT_STATUS_POS, timeout)
            if status == POZYX_SUCCESS:
                return self.getCoordinates(position)
            return status

    def doPositioningWithDataSlave(self, positioning_data, timeout=None):
        """Checks whether the device has positioned and if so, reads the position with data.

        This is useful for slave devices with a controller that needs to know the device's positions (with data) too

        Args:
            positioning_data: Container for the positioning coordinates. PositioningData object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        timeout = PozyxConstants.TIMEOUT_POSITIONING_DATA if timeout is None else timeout

        status = self.checkForFlag(PozyxBitmasks.INT_STATUS_POS, timeout)
        if status == POZYX_SUCCESS:
            return self.getPositioningData(positioning_data)
        return status

    ## @}

    def waitForFlagSafeFast(self, interrupt_flag, timeout_s, interrupt=None):
        """A fast variation of wait for flag, tripling the polling speed. Useful for ranging on very fast UWB settings.

        Returns:
            True, False
        """
        from time import time, sleep
        if interrupt is None:
            interrupt = SingleRegister()
        start = time()
        while (time() - start) < timeout_s:
            sleep(PozyxConstants.DELAY_POLLING * 0.33)
            status = self.getInterruptStatus(interrupt)
            if (interrupt[0] & interrupt_flag) and status == POZYX_SUCCESS:
                return True
        return False

    def checkForFlagFast(self, interrupt_flag, timeout_s, interrupt=None):
        """A fast variant of checkForFlag, using waitForFLagFast, useful for ranging on very fast UWB settings.

        Args:
            interrupt_flag: Flag of interrupt type to check the interrupt register against.
            timeout_s: duration to wait for the interrupt in seconds
            interrupt (optional): Container for the interrupt status register data.


        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if interrupt is None:
            interrupt = SingleRegister()
        error_interrupt_mask = PozyxBitmasks.INT_MASK_ERR
        if self.waitForFlagSafeFast(interrupt_flag | error_interrupt_mask, timeout_s, interrupt):
            if (interrupt[0] & error_interrupt_mask) == error_interrupt_mask:
                return POZYX_FAILURE
            else:
                return POZYX_SUCCESS
        else:
            return PozyxConstants.STATUS_TIMEOUT

    def remoteRegFunctionOnlyData(self, destination, address, params, data):
        """Performs a remote function without waiting for the acknowledgement.

        Advanded custom internal use only, you're not expected to use this unless you know what you're doing.

        """
        send_data = Data([0, address] + params.data, 'BB' + params.data_format)
        status = self.regFunction(PozyxRegisters.WRITE_TX_DATA, send_data, Data([]))
        if status != POZYX_SUCCESS:
            return status

        self.getInterruptStatus(SingleRegister())
        status = self.sendTXFunction(destination)
        if status != POZYX_SUCCESS:
            return status

        status = self.checkForFlagFast(PozyxBitmasks.INT_STATUS_RX_DATA, 1)
        if status == POZYX_SUCCESS:
            rx_info = RXInfo()
            self.getRxInfo(rx_info)
            if rx_info.remote_id == destination and rx_info.amount_of_bytes == 4:
                return_data = Data([0], 'I')
                self.readRXBufferData(return_data)
                return return_data[0]

        status = self.checkForFlagFast(PozyxBitmasks.INT_STATUS_RX_DATA, 1)
        if status == POZYX_SUCCESS:
            rx_info = RXInfo()
            self.getRxInfo(rx_info)
            if rx_info.remote_id == destination and rx_info.amount_of_bytes == 4:
                return_data = Data([0], 'I')
                self.readRXBufferData(return_data)
                return return_data[0]

    def rangingWithoutCheck(self, destination_id, device_range, remote_id=None):
        if not dataCheck(destination_id):
            destination_id = NetworkID(destination_id)

        if destination_id[0] < 0 or destination_id[0] > 0xFFFF:
            warn("Destination ID should be between 0x0000 and 0xFFFF, not {}".format(destination_id[0]))

        if remote_id is None:
            return self.doRanging(destination_id, device_range, remote_id=remote_id)

        distance = self.remoteRegFunctionOnlyData(remote_id, PozyxRegisters.DO_RANGING, destination_id, Data([]))

        if distance is not None:
            device_range.distance = distance
            return POZYX_SUCCESS
        return POZYX_FAILURE

    ## \addtogroup sensor_data
    # @{

    def getSensorMode(self, sensor_mode, remote_id=None):
        """Obtains the Pozyx's sensor mode.

        Args:
            sensor_mode: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.SENSORS_MODE , sensor_mode, remote_id)

    def getAllSensorData(self, sensor_data, remote_id=None):
        """Obtains all the Pozyx's sensor data in their default units.

        Args:
            sensor_data: Container for the read data. SensorData() or RawSensorData().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.PRESSURE, sensor_data, remote_id)

    def getPressure_Pa(self, pressure, remote_id=None):
        """Obtain the Pozyx's pressure sensor data in Pa(pascal).

        Args:
            pressure: Container for the read data. Pressure or Data([0], 'I') (Data is DEPRECATED).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getRead(PozyxRegisters.PRESSURE, pressure, remote_id)
        if pressure.__class__.__name__ == "Data":
            warn("Using Data instance in getPressure_Pa is deprecated, use Pressure instead",
                 DeprecationWarning)
            pressure[0] = pressure[0] / PozyxConstants.PRESSURE_DIV_PA
        return status

    def getMaxLinearAcceleration_mg(self, max_linear_acceleration, remote_id=None):
        """Obtain the Pozyx's acceleration sensor data in mg.

        Args:
            max_linear_acceleration: Container for the read data. MaxLinearAcceleration.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.MAX_LINEAR_ACCELERATION, max_linear_acceleration, remote_id)

    def getAcceleration_mg(self, acceleration, remote_id=None):
        """Obtain the Pozyx's acceleration sensor data in mg.

        Args:
            acceleration: Container for the read data. Acceleration().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.ACCELERATION_X, acceleration, remote_id)

    def getMagnetic_uT(self, magnetic, remote_id=None):
        """Obtain the Pozyx's magnetic sensor data in uT(microtesla).

        Args:
            magnetic: Container for the read data. Magnetic().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.MAGNETIC_X, magnetic, remote_id)

    def getAngularVelocity_dps(self, angular_vel, remote_id=None):
        """Obtain the Pozyx's angular velocity sensor data in dps(degrees per second).

        Args:
            angular_vel: Container for the read data. AngularVelocity().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.GYRO_X, angular_vel, remote_id)

    def getEulerAngles_deg(self, euler_angles, remote_id=None):
        """Obtain the Pozyx's euler angles sensor data in degrees(heading, roll, pitch).

        Args:
            euler_angles: Container for the read data. EulerAngles().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.EULER_ANGLE_HEADING, euler_angles, remote_id)

    def getNormalizedQuaternion(self, quaternion, remote_id=None):
        """Obtain the Pozyx's normalized quaternion sensor data that is required for ROS.

        Args:
            quaternion: Container for the read data. Quaternion().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getQuaternion(quaternion, remote_id)
        if status == POZYX_SUCCESS:
            quaternion.normalize()
        return status

    def getQuaternion(self, quaternion, remote_id=None):
        """Obtain the Pozyx's quaternion sensor data.

        Args:
            quaternion: Container for the read data. Quaternion().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.QUATERNION_W, quaternion, remote_id)

    def getLinearAcceleration_mg(self, linear_acceleration, remote_id=None):
        """Obtain the Pozyx's linear acceleration sensor data in mg.

        Args:
            linear_acceleration: Container for the read data. LinearAcceleration() or Acceleration().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.LINEAR_ACCELERATION_X, linear_acceleration, remote_id)

    def getGravityVector_mg(self, gravity_vector, remote_id=None):
        """Obtain the Pozyx's gravity vector sensor data in mg.

        Args:
            gravity_vector: Container for the read data. Acceleration().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.GRAVITY_VECTOR_X, gravity_vector, remote_id)

    def getTemperature_c(self, temperature, remote_id=None):
        """Obtain the Pozyx's temperature sensor data in C(celsius).

        Args:
            temperature: Container for the read data. Temperature or Data([0], 'b') (DEPRECATED).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.getRead(
            PozyxRegisters.TEMPERATURE, temperature, remote_id)
        if temperature.__class__.__name__ == "Data" or temperature.__class__.__name__ == "SingleRegister":
            warn("Using Data or SingleRegister instance in getTemperature_c is deprecated, use Temperature instead",
                 DeprecationWarning)
            temperature[0] = temperature[0] / PozyxConstants.TEMPERATURE_DIV_CELSIUS
        return status

    ##  @}

    ## \addtogroup device_list
    # @{

    def getDeviceListSize(self, device_list_size, remote_id=None):
        """Obtain the size of Pozyx's list of added devices.

        Args:
            device_list_size: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.DEVICE_LIST_SIZE, device_list_size, remote_id)

    def getDeviceIds(self, devices, remote_id=None):
        """Obtain the IDs of all devices in the Pozyx's device list.

        You need to make sure to know how many devices are in the list, as an incorrect
        size of anchors will cause the function to fail. Use getDeviceListSize
        to know this number.

        Args:
            devices: Container for the read data. DeviceList(list_size=size)
            or Data([0] * size, 'H' * size).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getAnchorIds, getTagIds, getPositioningAnchorIds

        Example:
            >> > list_size = SingleRegister()
            >> > self.getDeviceListSize(list_size)
            >> > device_list = DeviceList(list_size=list_size[0])
            >> > self.getDeviceIds(device_list)
            >> > print(device_list)
            '0x60A0, 0x6070, 0x6891'
        """
        if not 0 < len(devices) <= 20:
            warn("getDeviceIds: size {} not in range".format(len(devices)))

        list_size = SingleRegister()

        status = self.getDeviceListSize(list_size, remote_id)
        if list_size[0] < len(devices) or status == POZYX_FAILURE:
            return POZYX_FAILURE

        params = Data([0, list_size[0]])

        return self.useFunction(
            PozyxRegisters.GET_DEVICE_LIST_IDS, params, devices, remote_id)

    def getAnchorIds(self, anchors, remote_id=None):
        """Obtain the IDs of the anchors in the Pozyx's device list.

        You need to make sure to know how many anchors are in the list, as an incorrect
        size of anchors will cause the function to fail.

        Args:
            anchors: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getDeviceIds, getPositioningAnchorIds, getTagIds
        """
        if not 0 < len(anchors) <= 20:
            warn(f"getAnchorIds: size {len(anchors)} not in range")
        list_size = SingleRegister()
        status = self.getDeviceListSize(list_size, remote_id)
        if list_size[0] < len(anchors) or status == POZYX_FAILURE:
            return POZYX_FAILURE
        devices = DeviceList(list_size=list_size)
        status = self.useFunction(
            PozyxRegisters.GET_DEVICE_LIST_IDS, Data([]), devices, remote_id)

        if status == POZYX_SUCCESS:
            for i in range(len(anchors)):
                anchors[i] = 0x0
            j = 0
            for i in range(list_size[0]):
                data = Data([0] * 3)
                status &= self.useFunction(
                    PozyxRegisters.GET_DEVICE_INFO, NetworkID(devices[i]), data)
                # why + 1?
                if data[2] == PozyxConstants.ANCHOR_MODE + 1:
                    anchors[j] = devices[i]
                    j += 1
            # didn't find enough anchors, so the function failed.
            if j < len(anchors):
                return POZYX_FAILURE
        return status

    def getTagIds(self, tags, remote_id=None):
        """Obtain the IDs of the tags in the Pozyx's device list.

        You need to make sure to know how many tags are in the list, as an incorrect
        size of tags will cause the function to fail.

        Args:
            tags: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT

        See Also:
            getDeviceIds, getAnchorIds, getPositioningAnchorIds
        """
        if not 0 < len(tags) <= 20:
            warn(f"getTagIds: size {len(tags)} not in range")
        list_size = SingleRegister()
        status = self.getDeviceListSize(list_size, remote_id)
        if list_size[0] < len(tags) or status == POZYX_FAILURE:
            return POZYX_FAILURE
        devices = DeviceList(list_size=list_size[0])
        status = self.useFunction(
            PozyxRegisters.GET_DEVICE_LIST_IDS, Data([]), devices, remote_id)

        if status == POZYX_SUCCESS:
            for i in range(len(tags)):
                tags[i] = 0x0
            j = 0
            for i in range(list_size[0]):
                data = Data([0] * 3)
                status &= self.useFunction(
                    PozyxRegisters.GET_DEVICE_INFO, NetworkID(devices[i]), data)
                # why + 1?
                if data[2] == PozyxConstants.TAG_MODE + 1:
                    tags[j] = devices[i]
                    j += 1
            # didn't find enough tags, so the function failed.
            if j < len(tags):
                return POZYX_FAILURE
        return status

    def getDeviceCoordinates(self, device_id, coordinates, remote_id=None):
        """Obtain the coordinates of the device with selected ID in the Pozyx's device list.

        Args:
            device_id: ID of desired device whose coordinates are of interest. NetworkID()
            or Data([ID], 'H') or integer ID.
            coordinates: Container for the read data. Coordinates().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(device_id):
            device_id = NetworkID(device_id)
        if device_id[0] < 0 or device_id[0] > 0xFFFF:
            warn(f"getDeviceCoordinates: device ID should be between 0x0000 and 0xFFFF, not {device_id[0]}")
        return self.useFunction(
            PozyxRegisters.GET_DEVICE_COORDINATES, device_id, coordinates, remote_id)

    def doDiscovery(self, discovery_type=PozyxConstants.DISCOVERY_ANCHORS_ONLY, slots=3, slot_duration=0.01, remote_id=None):
        """Performs discovery on the Pozyx, which will let it discover other Pozyx devices with the same
        UWB settings in range.

        Args:
            discovery_type (optional): Type of devices to discover, defaults to discovering the anchors. PozyxConstants.DISCOVERY_ALL_DEVICES,
                PozyxConstants.DISCOVERY_TAGS_ONLY are alternatives.
            slots (optional): Number of timeslots used in attempt to discover devices. Default is 3 slots.
            slot_duration (optional): Duration in seconds of each timeslot used in the discovery process. Default is 10 ms.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if discovery_type not in PozyxConstants.DISCOVERY_TYPES:
            warn(f"doDiscovery: unknown discovery type {discovery_type}")
        if not 1 < slots < 10:
            warn(f"doDiscovery: slots should be between 1 and 10, not {slots}")
        if not (slot_duration == 0 or slot_duration >= 0.005):
            warn(f"doDiscovery: slot duration should be higher than 5ms, not {slot_duration * 1000}ms")

        self.getInterruptStatus(SingleRegister())
        params = Data([discovery_type, slots, int(slot_duration * 1000)])

        status = self.useFunction(
            PozyxRegisters.DO_DISCOVERY, params, remote_id=remote_id)
        if status == POZYX_FAILURE:
            return status
        timeout_s = slot_duration * (slots + 20)
        if remote_id is None:
            return self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, timeout_s)
        else:
            # give the remote device some time to perform its discovery.
            sleep(timeout_s)
        return status

    def doOptimalDiscovery(self, discovery_type=PozyxConstants.DISCOVERY_ALL_DEVICES, slots=3, timeout=None):
        """Performs a discovery with slot_duration optimised for the device's UWB settings."""
        self.getInterruptStatus(SingleRegister())
        timeout = PozyxConstants.TIMEOUT_OPTIMAL_DISCOVERY if timeout is None else timeout
        params = Data([discovery_type, slots, 0])
        status = self.useFunction(PozyxRegisters.DO_DISCOVERY, params)
        if status != PozyxConstants.STATUS_SUCCESS:
            return status
        return self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, timeout)

    def doDiscoveryTags(self, slots=3, slot_duration=0.01, remote_id=None):
        """Performs tag discovery on the Pozyx, which will let it discover Pozyx tags with the same
        UWB settings in range.

        Args:
            slots (optional): Number of timeslots used in attempt to discover devices. Default is 3 slots.
            slot_duration (optional): Duration in seconds of each timeslot used in the discovery process. Default is 10 ms.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.doDiscovery(discovery_type=PozyxConstants.DISCOVERY_TAGS_ONLY, slots=slots,
                                slot_duration=slot_duration, remote_id=remote_id)

    def doDiscoveryAnchors(self, slots=3, slot_duration=0.01, remote_id=None):
        """Performs anchor discovery on the Pozyx, which will let it discover Pozyx anchors with the same
        UWB settings in range.

        Args:
            slots (optional): Number of timeslots used in attempt to discover devices. Default is 3 slots.
            slot_duration (optional): Duration in seconds of each timeslot used in the discovery process. Default is 10 ms.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.doDiscovery(discovery_type=PozyxConstants.DISCOVERY_ANCHORS_ONLY, slots=slots,
                                slot_duration=slot_duration, remote_id=remote_id)

    def doDiscoveryAll(self, slots=3, slot_duration=0.01, remote_id=None):
        """Performs general discovery on the Pozyx, which will let it discover both Pozyx tags and anchors
        with the same UWB settings in range.

        Args:
            slots (optional): Number of timeslots used in attempt to discover devices. Default is 3 slots.
            slot_duration (optional): Duration in seconds of each timeslot used in the discovery process. Default is 10 ms.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.doDiscovery(discovery_type=PozyxConstants.DISCOVERY_ALL_DEVICES, slots=slots,
                                slot_duration=slot_duration, remote_id=remote_id)

    def doAnchorCalibration(self, dimension, num_measurements, anchors, heights=None, remote_id=None):
        """Performs automatic anchor calibration on the Pozyx.

        Using manual calibration over automatic calibration is highly recommended, as this will not only
        be less robust to use, the results will also be worse than a carefully accurately manually measured
        setup. Using a laser measurer for this purpose is also adviced.

        When insisting on using automatic calibration, make sure that all devices are in range and able to
        communicate with the device. Try ranging with all devices first, and make sure they're on the same
        UWB settings.

        Args:
            dimension: Dimension for the automatic calibration. When 2.5D, make sure to pass along heights as well.
            num_measurements: Number of measurements to use in calibration. The
            anchors: List of anchor IDs that will be used in the calibration. DeviceList() or [anchor_id1, anchor_id2, ...]
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        warn("Autocalibration will be removed from the library in favor of the much better external functionality in the Pozyx webapp.")
        if not dimension in PozyxConstants.DIMENSIONS:
            warn(f'doAnchorCalibration: wrong dimension {dimension}')
        if not num_measurements > 0:
            warn("doAnchorCalibration: a negative number of measurements isn\'t allowed")
        if not 3 <= len(anchors) <= 6:
            warn(f"doAnchorCalibration: num anchors {len(anchors)} out of range 3-6")

        if not dataCheck(anchors):
            anchors = DeviceList(anchors)
        if dimension == PozyxConstants.DIMENSION_2_5D:
            if heights is None:
                heights = [2000] * len(anchors)
            for i in range(len(anchors)):
                anchor_coordinates = Coordinates(0, 0, heights[i])
                anchor = DeviceCoordinates(anchors[i], 0x1, anchor_coordinates)
                self.addDevice(anchor, remote_id)

        self.getInterruptStatus(SingleRegister())
        params = Data([dimension, num_measurements] +
                      anchors.data, 'BB' + anchors.data_format)
        status = self.useFunction(0xC2, params, remote_id=remote_id)

        if remote_id is None:
            return self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, 25000)
        else:
            # give the remote device some time to perform calibration
            # has to be thoroughly tested
            sleep(1 * len(anchors) * num_measurements / 20)
        return status

    def clearDevices(self, remote_id=None):
        """Clears the Pozyx's device list.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.useFunction(PozyxRegisters.CLEAR_DEVICES, remote_id=remote_id)

    def addDevice(self, device_coordinates, remote_id=None):
        """Adds a device to the Pozyx's device list. Can be either a tag or anchor.

        Args:
            device_coordinates: Device's ID, flag, and coordinates structure. DeviceCoordinates(ID, flag, Coordinates(x, y, z)) or [ID, flag, x, y, z]
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(device_coordinates):
            device_coordinates = DeviceCoordinates(device_coordinates[0], device_coordinates[
                1], Coordinates(device_coordinates[2], device_coordinates[3], device_coordinates[4]))

        return self.useFunction(PozyxRegisters.ADD_DEVICE, device_coordinates, Data([]), remote_id)

    def saveNetwork(self, remote_id=None):
        """Saves the Pozyx's device list to its flash memory.

        This means that upon a reset, the Pozyx will still have the same configured device list.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        status = self.saveConfiguration(PozyxConstants.FLASH_SAVE_NETWORK, remote_id=remote_id)
        status &= self.saveRegisters([PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS],remote_id=remote_id)
        return status

    def configureAnchors(self, anchor_list, anchor_select=PozyxConstants.ANCHOR_SELECT_AUTO, remote_id=None):
        """Configures a set of anchors as the relevant anchors on a device

        Args:
            anchor_list (list): Python list of either DeviceCoordinates or [ID, flag, x, y, z]
            anchor_select (optional): How to select the anchors in positioning
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        status = POZYX_SUCCESS
        status &= self.clearDevices(remote_id)

        for anchor in anchor_list:
            if not dataCheck(anchor):
                anchor = DeviceCoordinates(
                    anchor[0], anchor[1], Coordinates(anchor[2], anchor[3], anchor[4]))
            if anchor.flag != 0x1:
                warn("ID 0x%0.4x added as tag, is this intentional?" % remote_id)
            status &= self.addDevice(anchor, remote_id)

        if len(anchor_list) < 3 or len(anchor_list) > 16:
            warn("Not enough anchors to do positioning")
            return status

        return status & self.setSelectionOfAnchors(anchor_select, len(anchor_list), remote_id)

    def removeDevice(self, device_id, remote_id=None):
        """Removes a device from the Pozyx's device list, keeping the rest of the list intact

        Args:
            device_id: ID that needs to be removed. NetworkID or integer.
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if dataCheck(device_id):
            device_id = device_id[0]

        status = POZYX_SUCCESS
        list_size = SingleRegister()
        status &= self.getDeviceListSize(list_size, remote_id)
        device_list = DeviceList(list_size=list_size[0])
        status &= self.getDeviceIds(device_list, remote_id)
        if device_id not in device_list or status != POZYX_SUCCESS:
            return POZYX_FAILURE
        devices = []
        for id_ in device_list:
            if id_ == device_id:
                continue
            coordinates = Coordinates()
            status = self.getDeviceCoordinates(id_, coordinates, remote_id)
            if status != POZYX_SUCCESS:
                return status
            devices.append(DeviceCoordinates(id_, 0x1, coordinates))

        anchor_select_mode = SingleRegister()

        status = self.getAnchorSelectionMode(anchor_select_mode, remote_id)
        if status != POZYX_SUCCESS:
            return status

        return self.configureAnchors(devices, anchor_select=anchor_select_mode, remote_id=remote_id)

    def changeDeviceCoordinates(self, device_id, new_coordinates, remote_id=None):
        """Changes a device's coordinates in the Pozyx's device list, keeping the rest of the list intact

        Args:
            device_id: ID that needs to be removed. NetworkID or integer.
            new_coordinates: new coordinates for the device
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if dataCheck(device_id):
            device_id = device_id[0]
        if not dataCheck(new_coordinates):
            new_coordinates = Coordinates(x=new_coordinates[0], y=new_coordinates[
                                          1], z=new_coordinates[2])
        status = POZYX_SUCCESS
        list_size = SingleRegister()
        status &= self.getDeviceListSize(list_size, remote_id)
        device_list = DeviceList(list_size=list_size[0])
        status &= self.getDeviceIds(device_list, remote_id)
        if device_id not in device_list or status is not POZYX_SUCCESS:
            return POZYX_FAILURE
        devices = []
        for id_ in device_list:
            if id_ == device_id:
                devices.append(DeviceCoordinates(id_, 0x1, new_coordinates))
                continue
            coordinates = Coordinates()
            self.getDeviceCoordinates(id_, coordinates, remote_id)
            devices.append(DeviceCoordinates(id_, 0x1, coordinates))

        anchor_select_mode = SingleRegister()
        self.getAnchorSelectionMode(anchor_select_mode, remote_id)

        self.configureAnchors(
            devices, anchor_select=anchor_select_mode, remote_id=remote_id)

    def printDeviceInfo(self, remote_id=None):
        """Prints a Pozyx's basic info, such as firmware.
    
        Mostly for debugging
        """
        firmware = SingleRegister()
        status = self.getFirmwareVersion(firmware, remote_id)

        network_id = NetworkID() if remote_id is None else NetworkID(remote_id)
        warn(f"Device information for device 0x{network_id.id:0.4x}")

        if status != POZYX_SUCCESS:
            warn("\t- Error: Couldn't retrieve device information")
        return

        warn(f"\t- Firmware version {firmware.value >> 4}.{firmware.value % 0x10}")

    def printDeviceList(self, remote_id=None, include_coordinates=True, prefix="\t- "):
        """Prints a Pozyx's device list.

        Args:
            remote_id (optional): Remote Pozyx ID
            include_coordinates (bool, optional): Whether to include coordinates in the prints
            prefix (str, optional): Prefix to prepend the device list

        Returns:
            None
        """
        list_size = SingleRegister()
        status = self.getDeviceListSize(list_size, remote_id)

        if list_size[0] == 0:
            return

        device_list = DeviceList(list_size=list_size[0])
        status &= self.getDeviceIds(device_list, remote_id)

        if status == POZYX_FAILURE:
            warn("Couldn't read device list of Pozyx")
            return

        for device_id in device_list:
            if include_coordinates:
                coordinates = Coordinates()
                self.getDeviceCoordinates(device_id, coordinates, remote_id)
                warn(f"{prefix}{DeviceCoordinates(device_id, 0x1, coordinates)}")
            else:
                warn(f"{prefix}0x{device_id:0.4x}")

    def saveUWBSettings(self, remote_id=None):
        """Saves the Pozyx's UWB settings to its flash memory.

        This means that upon a reset, the Pozyx will still have the same configured UWB settings.
        As of writing, PozyxRegisters.UWB_GAIN is not savable yet.

        Args:
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.saveRegisters(PozyxRegisters.ALL_UWB_REGISTERS, remote_id)

    def savePositioningSettings(self, remote_id=None):
        return self.saveRegisters(PozyxRegisters.ALL_POSITIONING_REGISTERS, remote_id)

    def setNetworkId(self, network_id, remote_id=None):
        """Set the Pozyx's network ID.

        If using this remotely, make sure to change the network ID to the new ID in
        subsequent code, as its ID will have changed and using the old ID will not work.

        Args:
            network_id: New Network ID. integer ID or NetworkID(ID) or SingleRegister(ID, size=2)
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(network_id):
            network_id = NetworkID(network_id)
        return self.setWrite(PozyxRegisters.NETWORK_ID, network_id, remote_id)

    def remoteRegFunctionWithoutCheck(self, destination, address, params):
        send_data = Data([0, address] + params.data, 'BB' + params.data_format)
        status = self.regFunction(PozyxRegisters.WRITE_TX_DATA, send_data, Data([]))
        if status == POZYX_FAILURE:
            return status

        self.getInterruptStatus(SingleRegister())

        status = self.sendTXFunction(destination)
        if status == POZYX_FAILURE:
            return status

        self.checkForFlag(PozyxBitmasks.INT_STATUS_FUNC, 0.02)

        return status

    def setUWBSettings(self, uwb_settings, remote_id=None, save_to_flash=False):
        """
        Set the Pozyx's UWB settings.

        If using this remotely, remember to change the local UWB settings as well
        to make sure you are still able to communicate with the remote device.

        Args:
            uwb_settings: The new UWB settings. UWBSettings() or [channel, bitrate, prf, plen, gain_db]

        Kwargs:
            remote_id: Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(uwb_settings):
            uwb_settings = UWBSettings(uwb_settings[0], uwb_settings[1],
                                       uwb_settings[2], uwb_settings[3], uwb_settings[4])
        gain_register = Data([uwb_settings.gain_db], 'f')
        uwb_registers = Data([uwb_settings.channel, uwb_settings.bitrate +
                    (uwb_settings.prf << 6), uwb_settings.plen])

        self.setWrite(PozyxRegisters.UWB_CHANNEL, uwb_registers, remote_id,
                      2 * PozyxConstants.DELAY_LOCAL_WRITE, 2 * PozyxConstants.DELAY_REMOTE_WRITE)

        if remote_id is None:
            status = self.setUWBGain(gain_register, remote_id)
            if save_to_flash:
                status &= self.saveUWBSettings()
        else:
            status = self.doFunctionOnDifferentUWB(self.setUWBGain, uwb_settings, gain_register, remote_id=remote_id)
            if save_to_flash:
                status &= self.doFunctionOnDifferentUWB(self.saveUWBSettings, uwb_settings, remote_id=remote_id)
        return status

    def checkUWBSettings(self, suspected_uwb_settings, remote_id=None, equal_gain=True):
        uwb = UWBSettings()

        if remote_id is None:
            self.getUWBSettings(uwb)
        else:
            self.doFunctionOnDifferentUWB(self.getUWBSettings, suspected_uwb_settings, uwb, remote_id=remote_id)

        if equal_gain is not True:
            uwb.gain_db = suspected_uwb_settings.gain_db
        return uwb == suspected_uwb_settings

    def doFunctionOnDifferentUWB(self, function, uwb_settings, *args, **kwargs):
        original_uwb_settings = UWBSettings()
        self.getUWBSettings(original_uwb_settings)

        self.setUWBSettings(uwb_settings)
        return_value = function(*args, **kwargs)
        self.setUWBSettings(original_uwb_settings)

        return return_value

    def setUWBChannel(self, channel_num, remote_id=None):
        """Set the Pozyx's UWB channel.

        If using this remotely, remember to change the local UWB channel as well
        to make sure you are still able to communicate with the remote device.

        Args:
            channel_num: The new UWB channel, being either 1, 2, 3, 4, 5 or 7.
                See PozyxRegisters.UWB_CHANNEL register. integer channel or SingleRegister(channel)
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(channel_num):
            channel_num = SingleRegister(channel_num)

        if not channel_num[0] in PozyxConstants.ALL_UWB_CHANNELS:
            warn(f"setUWBChannel: {channel_num[0]} is wrong channel number")

        return self.setWrite(PozyxRegisters.UWB_CHANNEL, channel_num, remote_id)

    def setUWBGain(self, uwb_gain_db, remote_id=None):
        """Set the Pozyx's UWB transceiver gain.

        Args:
            uwb_gain_db: The new transceiver gain in dB, a value between 0.0 and 33.0.
                float gain or Data([gain], 'f').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if not dataCheck(uwb_gain_db):
            uwb_gain_db = Data([uwb_gain_db], 'f')

        if not uwb_gain_db[0] >= 0.0 and uwb_gain_db[0] <= 33:
            warn("setUWBGain: TX gain {uwb_gain_db[0]}dB not in range (0-33dB)")

        doublegain_db = Data([int(2.0 * uwb_gain_db[0] + 0.5)])

        return self.setWrite(PozyxRegisters.UWB_GAIN, doublegain_db, remote_id)

    def setTxPower(self, txgain_db, remote_id=None):
        """DEPRECATED: use getUWBGain instead. Set the Pozyx's UWB transceiver gain.

        Args:
            txgain_db: The new transceiver gain in dB, a value between 0.0 and 33.0.
                float gain or Data([gain], 'f').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        warn("setTxPower is deprecated, use setUWBGain instead", DeprecationWarning)
        return self.setUWBGain(txgain_db, remote_id)

    def getNetworkId(self, network_id):
        """Obtains the Pozyx's network ID.

        Args:
            network_id: Container for the read data.  NetworkID() or SingleRegister(size=2) or Data([0], 'H').

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.regRead(PozyxRegisters.NETWORK_ID, network_id)

    def getUWBSettings(self, UWB_settings, remote_id=None):
        """Obtains the Pozyx's UWB settings.

        Args:
            UWB_settings: Container for the read data.  UWBSettings().
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        # The UWB data register size is 4.
        tmp_data = Data([0] * 4)
        status = self.getRead(PozyxRegisters.UWB_CHANNEL, tmp_data, remote_id)
        UWB_settings.load(tmp_data.data)
        return status

    def getUWBChannel(self, channel_num, remote_id=None):
        """Obtains the Pozyx's UWB channel.

        Args:
            channel_num: Container for the read data. SingleRegister or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        channel_num[0] = 0
        status = self.getRead(PozyxRegisters.UWB_CHANNEL, channel_num, remote_id)
        if channel_num[0] == 0 or status == POZYX_FAILURE:
            return POZYX_FAILURE
        return status

    def getUWBGain(self, uwb_gain_db, remote_id=None):
        """Obtains the Pozyx's transmitter UWB gain in dB, as a float.

        Args:
            uwb_gain_db: Container for the read data. Data([0], 'f').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        doublegain_db = SingleRegister()
        status = self.getRead(
            PozyxRegisters.UWB_GAIN, doublegain_db, remote_id)
        uwb_gain_db[0] = 0.5 * doublegain_db[0]
        return status

    def getTxPower(self, txgain_db, remote_id=None):
        """DEPRECATED: use getUWBGain instead. Obtains the Pozyx's transmitter UWB gain in dB, as a float.

        Args:
            txgain_db: Container for the read data. Data([0], 'f').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        warn("getTxPower is deprecated, use getUWBGain instead", DeprecationWarning)
        return self.getUWBGain(txgain_db, remote_id)

    def getLastNetworkId(self, network_id, remote_id=None):
        """Obtain the network ID of the last device Pozyx communicated with.

        Args:
            network_id: Container for the read data. NetworkID() or SingleRegister(size=2) or Data([0], 'H').
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.RX_NETWORK_ID, network_id, remote_id)

    def getLastDataLength(self, data_length, remote_id=None):
        """Obtain the size of the most recent data packet received by the Pozyx.

        Args:
            data_length: Container for the read data. SingleRegister() or Data([0]).
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        return self.getRead(PozyxRegisters.RX_DATA_LENGTH, data_length, remote_id)

    ## @}

    def saveConfiguration(self, save_type, registers=None, remote_id=None):
        """General function to save the Pozyx's configuration to its flash memory.

        This constitutes three different Pozyx configurations to save, and each have their specialised derived function:
            POZYX_FLASH_REGS: This saves the passed Pozyx registers if they're writable, see saveRegisters.
            PozyxConstants.FLASH_SAVE_ANCHOR_IDS: This saves the anchors used during positioning, see saveAnchorIds.
            POZYX_FLASH_NETWORK: This saves the device list to the Pozyx device, see saveNetwork.

        It is recommended to use the derived functions, as these are not just easier to use, but also
        more descriptive than this general save function.

        DISCLAIMER: Make sure to not abuse this function in your code, as the flash memory only has a finite
        number of writecycles available, adhere to the Arduino's mentality in using flash memory.

        Args:
            save_type: Type of configuration to save. See above.
            registers (optional): Registers to save to the flash memory. Data([register1, register2, ...]) or [register1, register2, ...]
                These registers have to be writable. Saving the UWB gain is currently not working.
            remote_id (optional): Remote Pozyx ID.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if registers is None:
            registers = Data([])
        if not dataCheck(registers):
            registers = Data(registers)

        if save_type not in PozyxConstants.ALL_FLASH_SAVE_TYPES:
            warn(f"saveConfiguration: unknown save flash type {save_type}")
        if save_type == PozyxConstants.FLASH_SAVE_REGISTERS and len(registers) == 0:
            warn("saveConfiguration: trying to save registers but none given")
        if save_type != PozyxConstants.FLASH_SAVE_REGISTERS and len(registers) > 0:
            warn("saveConfiguration: registers accidentally passed as parameters for non-register save")

        self.getInterruptStatus(SingleRegister())
        params = Data([save_type] + registers.data)
        status = self.useFunction(
            PozyxRegisters.SAVE_FLASH_MEMORY, params, remote_id=remote_id)
        if status != POZYX_SUCCESS:
            return status
        # give the device some time to save to flash memory
        sleep(PozyxConstants.DELAY_FLASH)
        return status

    def sendAloha(self, operation, remote_id=None):
        """Starts an ALOHA transmission with or without the custom payload or sets the custom payload to be sent on next transmission.

        Args:
            operation: POZYX_QUEUE_CUSTOM_ALOHA, POZYX_SEND_CUSTOM_ALOHA_IMMEDIATE, POZYX_SEND_ALOMA_IMMEDIATE
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if not dataCheck(operation):
            operation = SingleRegister(operation)

        if operation.value not in PozyxConstants.ALL_ALOHA_TYPES:
            warn("sendAloha: invalid ALOHA type {}".format(operation.value))

        return self.useFunction(PozyxRegisters.SEND_TX_DATA, operation, remote_id=remote_id)

    def getAlohaBlinkPayload(self, blink_payload, remote_id=None):
        return self.getRead(PozyxRegisters.CONFIG_BLINK_PAYLOAD, blink_payload, remote_id=remote_id)

    def setAlohaBlinkPayload(self, blink_payload, remote_id=None):
        """Sets the payload type that has to be sent when doing an ALOHA transmission.

        Args:
            payload_type: 
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if not dataCheck(blink_payload):
            blink_payload = Data([blink_payload], "H")

        return self.setWrite(PozyxRegisters.CONFIG_BLINK_PAYLOAD, blink_payload, remote_id=remote_id)

    def getAlohaBlinkInterval(self, interval, remote_id=None):
        return self.getRead(PozyxRegisters.POSITIONING_INTERVAL, interval, remote_id=remote_id)


    def setAlohaBlinkInterval(self, interval, remote_id=None):
        """Sets the interval in ms the aloha transmission has to wait to do an ALOHA transmission.

        Args:
            interval: the interval in ms 
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if not dataCheck(interval):
            interval = Data([interval], "H")

        return self.setWrite(PozyxRegisters.POSITIONING_INTERVAL, interval, remote_id=remote_id)

    def getAlohaBlinkVariation(self, variation, remote_id=None):
        return self.getRead(PozyxRegisters.ALOHA_VARIATION, variation, remote_id=remote_id)

    def setAlohaBlinkVariation(self, variation, remote_id=None):
        """Sets the interval in ms the aloha transmission has to wait to do an ALOHA transmission.

        Args:
            variation: the variation in ms
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        if not dataCheck(variation):
            variation = SingleRegister(variation)

        return self.setWrite(PozyxRegisters.ALOHA_VARIATION, variation, remote_id=remote_id)

    def startAloha(self, remote_id=None):
        """Sets the device in ALOHA mode.

        Args:
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.setOperationMode(PozyxConstants.ALOHA_MODE, remote_id=remote_id)

    def stopAloha(self, remote_id=None):
        """Returns the device to normal operation when in ALOHA mode.

        Args:
            remote_id (optional): Remote Pozyx ID

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.setOperationMode(PozyxConstants.TAG_MODE, remote_id=remote_id)

    def getBlinkIndex(self, index, remote_id=None):
        """Reads the blink index from ALOHA transmissions.

        Args:
            index: Data([0],'i')

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        return self.getRead(PozyxRegisters.BLINK_INDEX, index, remote_id=remote_id)

    def setOperationMode(self, mode, remote_id=None):
        """Sets the interval in ms the aloha transmission has to wait to do an ALOHA transmission.

                Args:
                    mode: the interval in ms
                    remote_id (optional): Remote Pozyx ID

                Returns:
                    POZYX_SUCCESS, POZYX_FAILURE
                """
        if not dataCheck(mode):
            mode = SingleRegister(mode)

        return self.setWrite(PozyxRegisters.OPERATION_MODE, mode, remote_id=remote_id)

class PozyxSerial(PozyxLib):
    """This class provides the Pozyx Serial interface, and opens and locks the serial
    port to use with Pozyx. All functionality from PozyxLib and PozyxCore is included.

    Args:
        port (str): Name of the serial port. On UNIX this will be '/dev/ttyACMX', on
            Windows this will be 'COMX', with X a random number.
        baudrate (optional): the baudrate of the serial port. Default value is 115200.
        timeout (optional): timeout for the serial port communication in seconds. Default is 0.1s or 100ms.
        print_output (optional): boolean for printing the serial exchanges, mainly for debugging purposes
        suppress_warnings (optional): boolean for suppressing warnings in the Pozyx use, usage not recommended
        debug_trace (optional): boolean for printing the trace on bad serial init (DEPRECATED)
        show_trace (optional): boolean for printing the trace on bad serial init (DEPRECATED)

    Example usage:
        >>> pozyx = PozyxSerial('COMX') # Windows
        >>> pozyx = PozyxSerial('/dev/ttyACMX', print_output=True) # Linux and OSX. Also puts debug output on.

    Finding the serial port can be easily done with the following code:
        >>> import serial.tools.list_ports
        >>> print serial.tools.list_ports.comports()[0]

    Putting one and two together, automating the correct port selection with one Pozyx attached:
        >>> import serial.tools.list_ports
        >>> pozyx = PozyxSerial(serial.tools.list_ports.comports()[0])
    """

    # \addtogroup core
    # @{
    def __init__(self, port, baudrate=115200, timeout=0.1, write_timeout=0.1,
                 print_output=False, debug_trace=False, show_trace=False,
                 suppress_warnings=False):
        """Initializes the PozyxSerial object. See above for details."""
        super(PozyxSerial, self).__init__()
        self.print_output = print_output
        if debug_trace is True or show_trace is True:
            if not suppress_warnings:
                warn("debug_trace or show_trace are on their way out, exceptions of the type PozyxException are now raised.",
                     DeprecationWarning)
        self.suppress_warnings = suppress_warnings

        self.connectToPozyx(port, baudrate, timeout, write_timeout)

        sleep(0.25)

        self.validatePozyx()

    def connectToPozyx(self, port, baudrate, timeout, write_timeout):
        """Attempts to connect to the Pozyx via a serial connection"""
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.write_timeout = write_timeout

        try:
            serial_params = {
                'port': port,
                'baudrate': baudrate,
                'timeout': timeout,
                'write_timeout': write_timeout
            }

            if is_correct_pyserial_version():
                if not is_pozyx(port) and not self.suppress_warnings:
                    warn(f"The passed device is not a recognized Pozyx device, is {get_port_object(port).description}", stacklevel=2)
                self.ser = Serial(**serial_params)
            else:
                if not self.suppress_warnings:
                    warn(f"PySerial version {PYSERIAL_VERSION} not supported, please upgrade to 3.0 or (preferably) higher", stacklevel=0)
                serial_params['writeTimeout'] = write_timeout
                self.ser = Serial(**serial_params)
        except SerialException as exc:
            raise PozyxConnectionError(f"Wrong or busy serial port, SerialException: {str(exc)}")
        except Exception as exc:
            raise PozyxConnectionError(f"Couldn't connect to Pozyx, {exc.__class__.__name__}: {str(exc)}")

    def validatePozyx(self):
        """Validates whether the connected device is indeed a Pozyx device"""
        whoami = SingleRegister()
        if self.getWhoAmI(whoami) != POZYX_SUCCESS:
            raise PozyxConnectionError("Connected to device, but couldn't read serial data. Is it a Pozyx?")
        if whoami.value != 0x43:
            raise PozyxConnectionError("POZYX_WHO_AM_I returned 0x%0.2x, something is wrong with Pozyx." % whoami.value)


    def regWrite(self, address, data):
        """
        Writes data to the Pozyx registers, starting at a register address,
        if registers are writable.

        Args:
            address: Register address to start writing at.
            data: Data to write to the Pozyx registers.
                Has to be ByteStructure-derived object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        data.load_hex_string()
        index = 0
        runs = int(data.byte_size / MAX_SERIAL_SIZE)
        for i in range(runs):
            s = 'W,%0.2x,%s\r' % (
                address + index, data.byte_data[2 * index: 2 * (index + MAX_SERIAL_SIZE)])
            index += MAX_SERIAL_SIZE
            try:
                self.ser.write(s.encode())
            except SerialException:
                return POZYX_FAILURE
            # delay(POZYX_DELAY_LOCAL_WRITE)
        s = 'W,%0.2x,%s\r' % (address + index, data.byte_data[2 * index:])
        try:
            self.ser.write(s.encode())
        except SerialException:
            return POZYX_FAILURE
        return POZYX_SUCCESS

    def serialExchange(self, s):
        """
        Auxiliary. Performs a serial write to and read from the Pozyx.

        Args:
            s: Serial message to send to the Pozyx
        Returns:
            Serial message the Pozyx returns, stripped from 'D,' at its start
                and NL+CR at the end.
        """
        self.ser.write(s.encode())
        response = self.ser.readline().decode()
        if self.print_output:
            print('The response to %s is %s.' % (s.strip(), response.strip()))
        if len(response) == 0:
            raise SerialException
        if response[0] == 'D':
            return response[2:-2]
        raise SerialException

    def regRead(self, address, data):
        """
        Reads data from the Pozyx registers, starting at a register address,
        if registers are readable.

        Args:
            address: Register address to start writing at.
            data: Data to write to the Pozyx registers.
                Has to be ByteStructure-derived object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        runs = int(data.byte_size / MAX_SERIAL_SIZE)
        r = ''
        for i in range(runs):
            s = 'R,%0.2x,%i\r' % (
                address + i * MAX_SERIAL_SIZE, MAX_SERIAL_SIZE)
            try:
                r += self.serialExchange(s)
            except SerialException:
                return POZYX_FAILURE
        s = 'R,%0.2x,%i\r' % (
            address + runs * MAX_SERIAL_SIZE, data.byte_size - runs * MAX_SERIAL_SIZE)
        try:
            r += self.serialExchange(s)
        except SerialException:
            return POZYX_FAILURE
        data.load_bytes(r)
        return POZYX_SUCCESS

    def regFunction(self, address, params, data):
        """
        Performs a register function on the Pozyx, if the address is a register
        function.

        Args:
            address: Register function address of function to perform.
            params: Parameters for the register function.
                Has to be ByteStructure-derived object.
            data: Container for the data the register function returns.
                Has to be ByteStructure-derived object.

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE
        """
        params.load_hex_string()
        s = 'F,%0.2x,%s,%i\r' % (address, params.byte_data, data.byte_size + 1)
        try:
            r = self.serialExchange(s)
        except SerialException:
            return POZYX_FAILURE
        if len(data) > 0:
            data.load_bytes(r[2:])
        return int(r[0:2], 16)

    def waitForFlag(self, interrupt_flag, timeout_s, interrupt=None):
        """
        Waits for a certain interrupt flag to be triggered, indicating that
        that type of interrupt occured.

        Args:
            interrupt_flag: Flag indicating interrupt type.
            timeout_s: time in seconds that POZYX_INT_STATUS will be checked
                for the flag before returning POZYX_TIMEOUT.

        Kwargs:
            interrupt: Container for the POZYX_INT_STATUS data

        Returns:
            POZYX_SUCCESS, POZYX_FAILURE, POZYX_TIMEOUT
        """
        if interrupt is None:
            interrupt = SingleRegister()
        return self.waitForFlagSafe(interrupt_flag, timeout_s, interrupt)

class ReadyToLocalize(object):
    """Continuously calls the Pozyx positioning function and prints its position."""

    def __init__(self, pozyx, osc_udp_client, anchors, algorithm=POZYX_POS_ALG_UWB_ONLY, dimension=POZYX_3D, height=1000, remote_id=None):
        self.pozyx = pozyx
        self.osc_udp_client = osc_udp_client

        self.anchors = anchors
        self.algorithm = algorithm
        self.dimension = dimension
        self.height = height
        self.remote_id = remote_id

    def setup(self):
        """Sets up the Pozyx for positioning by calibrating its anchor list."""
        
        self.setAnchorsManual(save_to_flash=False)
        self.printPublishConfigurationResult()

    def loop(self):
        """Performs positioning and displays/exports the results."""
        position = Coordinates()
        status = self.pozyx.doPositioning(
            position, self.dimension, self.height, self.algorithm, remote_id=self.remote_id)
        if status == POZYX_SUCCESS:
            self.printPublishPosition(position)
        else:
            self.printPublishErrorCode("positioning")

    def printPublishPosition(self, position):
        """Prints the Pozyx's position and possibly sends it as a OSC packet"""
        network_id = self.remote_id
        if network_id is None:
            network_id = 0
        warn("POS ID {}, x(mm): {pos.x} y(mm): {pos.y} z(mm): {pos.z}".format(
            "0x%0.4x" % network_id, pos=position))
        if self.osc_udp_client is not None:
            self.osc_udp_client.send_message(
                "/position", [network_id, int(position.x), int(position.y), int(position.z)])
    
    def printPublishConfigurationResult(self):
            """Prints and potentially publishes the anchor configuration result in a human-readable way."""
            list_size = SingleRegister()

            self.pozyx.getDeviceListSize(list_size, self.remote_id)
            print(f"List size: {list_size[0]}")
            if list_size[0] != len(self.anchors):
                self.printPublishErrorCode("configuration")
                return
            device_list = DeviceList(list_size=list_size[0])
            self.pozyx.getDeviceIds(device_list, self.remote_id)
            print(f"Calibration result:\nAnchors found: {list_size[0]}\n Anchor IDs: {device_list}")

            for i in range(list_size[0]):
                anchor_coordinates = Coordinates()
                self.pozyx.getDeviceCoordinates(device_list[i], anchor_coordinates, self.remote_id)
                print("ANCHOR, 0x%0.4x, %s" % (device_list[i], str(anchor_coordinates)))

    

    def printPublishErrorCode(self, operation):
        """Prints the Pozyx's error and possibly sends it as a OSC packet"""
        error_code = SingleRegister()
        network_id = self.remote_id
        if network_id is None:
            self.pozyx.getErrorCode(error_code)
            print("LOCAL ERROR %s, %s" % (operation, self.pozyx.getErrorMessage(error_code)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message("/error", [operation, 0, error_code[0]])
            return
        status = self.pozyx.getErrorCode(error_code, self.remote_id)
        if status == POZYX_SUCCESS:
            print("ERROR %s on ID %s, %s" %
                  (operation, "0x%0.4x" % network_id, self.pozyx.getErrorMessage(error_code)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message(
                    "/error", [operation, network_id, error_code[0]])
        else:
            self.pozyx.getErrorCode(error_code)
            print("ERROR %s, couldn't retrieve remote error code, LOCAL ERROR %s" %
                  (operation, self.pozyx.getErrorMessage(error_code)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message("/error", [operation, 0, -1])
            # should only happen when not being able to communicate with a remote Pozyx.
    
    def setAnchorsManual(self, save_to_flash=False):
        """Adds the manually measured anchors to the Pozyx's device list one for one."""
        status = self.pozyx.clearDevices(remote_id=self.remote_id)
        for anchor in self.anchors:
            status &= self.pozyx.addDevice(anchor, remote_id=self.remote_id)
        return status

class Coordinates(XYZ):
    """Container for coordinates in x, y, and z (in mm)."""
    byte_size = 12
    data_format = 'iii'

    def load(self, data, convert=False):
        self.data = data

class Acceleration(XYZ):
    """Container for acceleration in x, y, and z (in mg)."""
    physical_convert = PozyxConstants.ACCELERATION_DIV_MG

    byte_size = 6
    data_format = 'hhh'

class Magnetic(XYZ):
    """Container for coordinates in x, y, and z (in uT)."""
    physical_convert = PozyxConstants.MAGNETOMETER_DIV_UT

    byte_size = 6
    data_format = 'hhh'

class AngularVelocity(XYZ):
    """Container for angular velocity in x, y, and z (in dps)."""
    physical_convert = PozyxConstants.GYRO_DIV_DPS

    byte_size = 6
    data_format = 'hhh'

class LinearAcceleration(XYZ):
    """Container for linear acceleration in x, y, and z (in mg), as floats."""
    physical_convert = PozyxConstants.ACCELERATION_DIV_MG

    byte_size = 6
    data_format = 'hhh'

class PositionError(ByteStructure):
    """Container for position error in x, y, z, xy, xz, and yz (in mm)."""
    physical_convert = 1
    byte_size = 12
    data_format = 'hhhhhh'

    def __init__(self, x=0, y=0, z=0, xy=0, xz=0, yz=0):
        """Initializes the PositionError object."""
        self.data = [x, y, z, xy, xz, yz]

    def load(self, data, convert=False):
        self.data = data

    def __str__(self):
        return f'X: {self.x}, Y: {self.y}, Z: {self.z}, XY: {self.xy}, XZ: {self.xz}, YZ: {self.yz}'

    @property
    def x(self):
        return self.data[0] / self.physical_convert

    @x.setter
    def x(self, value):
        self.data[0] = value * self.physical_convert

    @property
    def y(self):
        return self.data[1] / self.physical_convert

    @y.setter
    def y(self, value):
        self.data[1] = value * self.physical_convert

    @property
    def z(self):
        return self.data[2] / self.physical_convert

    @z.setter
    def z(self, value):
        self.data[2] = value * self.physical_convert

    @property
    def xy(self):
        return self.data[3] / self.physical_convert

    @xy.setter
    def xy(self, value):
        self.data[3] = value * self.physical_convert

    @property
    def xz(self):
        return self.data[4] / self.physical_convert

    @xz.setter
    def xz(self, value):
        self.data[4] = value * self.physical_convert

    @property
    def yz(self):
        return self.data[5] / self.physical_convert

    @yz.setter
    def yz(self, value):
        self.data[5] = value * self.physical_convert

class Quaternion(ByteStructure):
    """Container for quaternion data in x, y, z and w."""
    physical_convert = PozyxConstants.QUATERNION_DIV

    byte_size = 8
    data_format = 'hhhh'

    def __init__(self, w=0, x=0, y=0, z=0):
        """Initializes the Quaternion object."""
        self.data = [w, x, y, z]

    def load(self, data, convert=True):
        for i in range(len(data)):
            data[i] = float(data[i])
        self.data = data

    def get_sum(self):
        """Returns the normalization value of the quaternion"""
        return sqrt((self.x * self.x) + (self.y * self.y) + (self.z * self.z) + (self.w * self.w))

    def normalize(self):
        """Normalizes the quaternion's data"""
        sum = self.get_sum()
        for i in range(len(self.data)):
            self.data[i] /= sum

    def __str__(self):
        return f'X: {self.x}, Y: {self.y}, Z: {self.z}, W: {self.w}'

    @property
    def w(self):
        return self.data[0] / self.physical_convert

    @w.setter
    def w(self, value):
        self.data[0] = value * self.physical_convert

    @property
    def x(self):
        return self.data[1] / self.physical_convert

    @x.setter
    def x(self, value):
        self.data[1] = value * self.physical_convert

    @property
    def y(self):
        return self.data[2] / self.physical_convert

    @y.setter
    def y(self, value):
        self.data[2] = value * self.physical_convert

    @property
    def z(self):
        return self.data[3] / self.physical_convert

    @z.setter
    def z(self, value):
        self.data[3] = value * self.physical_convert

    def to_dict(self):
        return {
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "w": self.w,
        }

class MaxLinearAcceleration(SingleSensorValue):
    physical_convert = PozyxConstants.MAX_LINEAR_ACCELERATION_DIV_MG

    byte_size = 2
    data_format = 'h'

class Temperature(SingleSensorValue):
    physical_convert = PozyxConstants.TEMPERATURE_DIV_CELSIUS

    byte_size = 1
    data_format = 'b'

    def __str__(self):
        return "{self.value} °C"

class Pressure(SingleSensorValue):
    physical_convert = PozyxConstants.PRESSURE_DIV_PA

    byte_size = 4
    data_format = 'I'

class EulerAngles(ByteStructure):
    """Container for euler angles as heading, roll, and pitch (in degrees)."""
    physical_convert = PozyxConstants.EULER_ANGLES_DIV_DEG

    byte_size = 6
    data_format = 'hhh'

    def __init__(self, heading=0, roll=0, pitch=0):
        """Initializes the EulerAngles object."""
        self.data = [heading, roll, pitch]

    def load(self, data, convert=True):
        self.data = data

    def __str__(self):
        return f'Heading: {self.heading}, Roll: {self.roll}, Pitch: {self.pitch}'

    @property
    def heading(self):
        return self.data[0] / self.physical_convert

    @heading.setter
    def heading(self, value):
        self.data[0] = value * self.physical_convert

    @property
    def roll(self):
        return self.data[1] / self.physical_convert

    @roll.setter
    def roll(self, value):
        self.data[1] = value * self.physical_convert

    @property
    def pitch(self):
        return self.data[2] / self.physical_convert

    @pitch.setter
    def pitch(self, value):
        self.data[2] = value * self.physical_convert

class DeviceCoordinates(ByteStructure):
    """
    Container for both reading and writing device coordinates from and to Pozyx.

    The keyword arguments are at once its properties.

    Kwargs:
        network_id: Network ID of the device
        flag: Type of the device. Tag or anchor.
        pos: Coordinates of the device. Coordinates().
    """
    byte_size = 15
    data_format = 'HBiii'

    def __init__(self, network_id=0, flag=0, pos=Coordinates()):
        """
        Initializes the DeviceCoordinates object.

        Kwargs:
            network_id: Network ID of the device
            flag: Type of the device. Tag or anchor.
            pos: Coordinates of the device. Coordinates().
        """
        self.data = [network_id, flag, int(pos.x), int(pos.y), int(pos.z)]

    def load(self, data):
        self.data = data

    def __str__(self):
        return "ID: 0x{:04X}, flag: {}, ".format(self.network_id, self.flag) + str(self.pos)

    @property
    def network_id(self):
        return self.data[0]

    @network_id.setter
    def network_id(self, value):
        self.data[0] = value

    @property
    def flag(self):
        return self.data[1]

    @flag.setter
    def flag(self, value):
        self.data[1] = value

    @property
    def pos(self):
        return Coordinates(self.data[2], self.data[3], self.data[4])

    @pos.setter
    def pos(self, value):
        self.data[2] = value.x
        self.data[3] = value.y
        self.data[4] = value.z

class DeviceRange(ByteStructure):
    """
    Container for the device range data, resulting from a range measurement.

    The keyword arguments are at once its properties.

    Kwargs:
        timestamp: Timestamp of the range measurement
        distance: Distance measured by the device.
        RSS: Signal strength during the ranging measurement.
    """
    byte_size = 10
    data_format = 'IIh'

    # TODO should ideally be rss not RSS
    def __init__(self, timestamp=0, distance=0, RSS=0):
        """Initializes the DeviceRange object."""
        self.data = [timestamp, distance, RSS]

    def load(self, data):
        self.data = data

    def __str__(self):
        return f'{self.timestamp} ms, {self.distance} mm, {self.RSS} dBm'

    @property
    def timestamp(self):
        return self.data[0]

    @timestamp.setter
    def timestamp(self, value):
        self.data[0] = value

    @property
    def distance(self):
        return self.data[1]

    @distance.setter
    def distance(self, value):
        self.data[1] = value

    @property
    def RSS(self):
        return self.data[2]

    @RSS.setter
    def RSS(self, value):
        self.data[2] = value

def parse_id_str(id_):
    if id_ is None:
        return None
    if isinstance(id_, str):
        if id_.startswith("0x"):
            return str(int(id_, 16))
        else:
            return id_
    elif isinstance(id_, int):
        return str(id_)

def parse_id_int(id_):
    if id_ is None:
        return None
    return int(parse_id_str(id_))

class NetworkID(SingleRegister):
    """
    Container for a device's network ID.

    Kwargs:
        network_id: The network ID of the device.
    """

    def __init__(self, network_id=0):
        """Initializes the NetworkID object."""
        super(NetworkID, self).__init__(parse_id_int(network_id), 2, signed=False)

    def __str__(self):
        return "0x{:04X}".format(self.id)

    @property
    def id(self):
        return self.data[0]

    @id.setter
    def id(self, value):
        self.data[0] = value

    @property
    def network_id(self):
        return self.data[0]

    @network_id.setter
    def network_id(self, value):
        self.data[0] = value

class DeviceList(Data):
    """
    Container for a list of device IDs.

    Using list_size is recommended when having just used getDeviceListSize, while ids
    is recommended when one knows the IDs. When using one, the other automatically
    gets its respective value. Therefore, only use on of both.

    Note also that DeviceList(list_size=1) is the same as NetworkID().

    Kwargs:
        ids: Array of known or unknown device IDs. Empty by default.
        list_size: Size of the device list.
    """

    def __init__(self, ids=None, list_size=0):
        """Initializes the DeviceList object with either IDs or its size."""
        ids = [] if ids is None else ids
        if list_size != 0 and ids == []:
            Data.__init__(self, [0] * list_size, 'H' * list_size)
        else:
            Data.__init__(self, ids, 'H' * len(ids))

    def __str__(self):
        return 'IDs: ' + ', '.join([str(id) for id in self])

    def load(self, data, convert=False):
        self.data = data

    @property
    def list_size(self):
        return len(self.data)

class RXInfo(ByteStructure):
    byte_size = 3
    data_format = 'HB'

    def __init__(self, remote_id=0, amount_of_bytes=0):
        """Initialises the RX Info structure"""
        self.data = [remote_id, amount_of_bytes]

    def load(self, data):
        self.data = data

    @property
    def remote_id(self):
        return self.data[0]

    @remote_id.setter
    def remote_id(self, value):
        self.data[0] = value

    @property
    def amount_of_bytes(self):
        return self.data[1]

    @amount_of_bytes.setter
    def amount_of_bytes(self, value):
        self.data[1] = value

class TXInfo(ByteStructure):
    """Container for data transmission meta information

    Args:
        remote_id: ID to transmit to
        operation: remote operation to execute
    """
    byte_size = 3
    data_format = 'HB'

    def __init__(self, remote_id, operation=PozyxConstants.REMOTE_DATA):
        self.data = [remote_id, operation]

    @property
    def remote_id(self):
        return self.data[0]

    @remote_id.setter
    def remote_id(self, value):
        self.data[0] = value

    @property
    def operation(self):
        return self.data[1]

    @operation.setter
    def operation(self, value):
        self.data[1] = value

class UWBMapping:
    BITRATES = {
        0: '110 kbit/s', 
        1: '850 kbit/s', 
        2: '6.81 Mbit/s'
        }
    PRFS = {
        1: '16 MHz', 
        2: '64 MHz'
        }
    PREAMBLE_LENGTHS = {
        0x0C: '4096 symbols', 
        0x28: '2048 symbols', 
        0x18: '1536 symbols', 
        0x08: '1024 symbols',
        0x34: '512 symbols', 
        0x24: '256 symbols', 
        0x14: '128 symbols', 
        0x04: '64 symbols'
        }

class UWBSettings(ByteStructure):
    """
    Container for a device's UWB settings.

    Its keyword arguments are at once its properties.

    It also provides parsing functions for all its respective properties,
    which means this doesn't need to be done by users. These functions are
    parse_prf, parse_plen and parse_bitrate.

    You can also directly print the UWB settings, resulting in the following
    example output:
    "CH: 1, bitrate: 850kbit/s, prf: 16MHz, plen: 1024 symbols, gain: 15.0dB"

    Kwargs:
        channel: UWB channel of the device. See POZYX_UWB_CHANNEL.
        bitrate: Bitrate of the UWB commmunication. See POZYX_UWB_RATES.
        prf: Pulse repeat frequency of the UWB. See POZYX_UWB_RATES.
        plen: Preamble length of the UWB packets. See POZYX_UWB_PLEN.
        gain_db: Gain of the UWB transceiver, a float value. See POZYX_UWB_GAIN.
    """
    byte_size = 7
    data_format = 'BBBBf'

    def __init__(self, channel=0, bitrate=0, prf=0, plen=0, gain_db=0.0):
        """Initializes the UWB settings."""
        self.data = [channel, bitrate + (prf << 6), plen, int(2 * gain_db)]

    def load(self, data, convert=False):
        self.data = data

    def parse_bitrate(self):
        """Parses the bitrate to be humanly readable."""
        try:
            return UWBMapping.BITRATES[self.bitrate]
        except KeyError:
            return 'invalid bitrate'

    def parse_prf(self):
        """Parses the pulse repetition frequency to be humanly readable."""

        try:
            return UWBMapping.PRFS[self.prf]
        except KeyError:
            return 'invalid pulse repetitions frequency (PRF)'

    def parse_plen(self):
        """Parses the preamble length to be humanly readable."""

        try:
            return UWBMapping.PREAMBLE_LENGTHS[self.plen]
        except KeyError:
            return 'invalid preamble length'

    def __str__(self):
        return f"CH: {self.channel}, bitrate: {self.parse_bitrate()}, prf: {self.parse_prf()}, plen: {self.parse_plen()}, gain: {self.gain_db} dB"

    @property
    def channel(self):
        return self.data[0]

    @channel.setter
    def channel(self, value):
        self.data[0] = value

    @property
    def bitrate(self):
        return self.data[1] & 0x3F

    @bitrate.setter
    def bitrate(self, value):
        self.data[1] = (self.data[1] & 0xC0) + value

    @property
    def prf(self):
        return (self.data[1] & 0xC0) >> 6

    @prf.setter
    def prf(self, value):
        self.data[1] = (self.data[1] & 0x3F) + (value << 6)

    @property
    def plen(self):
        return self.data[2]

    @plen.setter
    def plen(self, value):
        self.data[2] = value

    @property
    def gain_db(self):
        return float(self.data[3]) / 2

    @gain_db.setter
    def gain_db(self, value):
        self.data[3] = int(value * 2)

#------------------------------------------------------------------------------------------------------
class FilterData(ByteStructure):
    byte_size = 1
    data_format = 'B'

    def __init__(self, filter_type=0, filter_strength=0):
        self.data = [filter_type + (filter_strength << 4)]

    def load(self, data, convert=False):
        self.data = data

    def get_filter_name(self):
        filter_types = {
            PozyxConstants.FILTER_TYPE_NONE: "No filter",
            PozyxConstants.FILTER_TYPE_FIR: "FIR filter",
            PozyxConstants.FILTER_TYPE_MOVING_AVERAGE: "Moving average filter",
            PozyxConstants.FILTER_TYPE_MOVING_MEDIAN: "Moving median filter",
        }
        return filter_types.get(self.filter_type, "Unknown filter {}".format(self.filter_type))

    def __str__(self):
        return "{} with strength {}".format(self.get_filter_name(), self.filter_strength)
    
    @property
    def value(self):
        return self.data[0]
    
    @value.setter
    def value(self, value):
        self.data[0] = value

    @property
    def filter_type(self):
        return self.data[0] & 0xF

    @filter_type.setter
    def filter_type(self, value):
        self.data[0] = value + (self.filter_strength << 4)

    @property
    def filter_strength(self):
        return self.data[0] >> 4

    @filter_strength.setter
    def filter_strength(self, value):
        self.data[0] = self.filter_type + (value << 4)
#--------------------------------------------------------------------------------------
class AlgorithmData(ByteStructure):
    byte_size = 1
    data_format = 'B'

    def __init__(self, algorithm=0, dimension=0):
        self.data = [algorithm + (dimension << 4)]

    def load(self, data, convert=False):
        self.data = data

    def get_algorithm_name(self):
        algorithms = {
            PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY: "UWB only",
            PozyxConstants.POSITIONING_ALGORITHM_TRACKING: "Tracking",
            PozyxConstants.POSITIONING_ALGORITHM_NONE: "None",
        }
        return algorithms.get(self.algorithm, "Unknown filter {}".format(self.algorithm))

    def get_dimension_name(self):
        dimensions = {
            PozyxConstants.DIMENSION_2D: "2D",
            PozyxConstants.DIMENSION_2_5D: "2.5D",
            PozyxConstants.DIMENSION_3D: "3D",
        }
        return dimensions.get(self.dimension, "Unknown filter {}".format(self.algorithm))

    def __str__(self):
        return "Algorithm {}, dimension {}".format(self.get_algorithm_name(), self.get_dimension_name())

    @property
    def value(self):
        return self.data[0]

    @value.setter
    def value(self, value):
        self.data[0] = value

    @property
    def algorithm(self):
        return self.data[0] & 0xF

    @algorithm.setter
    def algorithm(self, value):
        self.data[0] = value + (self.dimension << 4)

    @property
    def dimension(self):
        return self.data[0] >> 4

    @dimension.setter
    def dimension(self, value):
        self.data[0] = self.algorithm + (value << 4)
#-----------------------------------------------------------------------------------------------------------------------
class SensorData(ByteStructure):
    """
    Container for all sensor data.

    This includes, in order, with respective structure:
        - pressure : UInt32
        - acceleration : Acceleration
        - magnetic : Magnetic
        - angular_vel : AngularVelocity
        - euler_angles : EulerAngles
        - quaternion : Quaternion
        - linear_acceleration: LinearAcceleration
        - gravity_vector: LinearAcceleration
        - temperature: Int8
    """
    byte_size = 49  # 4 + 6 + 6 + 6 + 6 + 8 + 6 + 6 + 1

    # 'I' + 'h'*3 + 'h'*3 + 'h'*3 + 'h'*3 + 'f'*4 + 'h'*3 + 'h'*3 + 'b'
    data_format = 'Ihhhhhhhhhhhhhhhhhhhhhhb'

    def __init__(self, data=[0] * 24):
        """Initializes the SensorData object."""
        self.data = data
        self.pressure = Pressure(data[0])
        self.acceleration = Acceleration(data[1], data[2], data[3])
        self.magnetic = Magnetic(data[4], data[5], data[6])
        self.angular_vel = AngularVelocity(data[7], data[8], data[9])
        self.euler_angles = EulerAngles(data[10], data[11], data[12])
        self.quaternion = Quaternion(data[13], data[14], data[15], data[16])
        self.linear_acceleration = LinearAcceleration(
            data[17], data[18], data[19])
        self.gravity_vector = LinearAcceleration(data[20], data[21], data[22])
        self.temperature = Temperature(data[23])

    def load(self, data, convert=True):
        self.data = data
        self.pressure.load([data[0]], convert)
        self.acceleration.load(data[1:4], convert)
        self.magnetic.load(data[4:7], convert)
        self.angular_vel.load(data[7:10], convert)
        self.euler_angles.load(data[10:13], convert)
        self.quaternion.load(data[13:17], convert)
        self.linear_acceleration.load(data[17:20], convert)
        self.gravity_vector.load(data[20:23], convert)
        self.temperature.load([data[23]], convert)

class RawSensorData(SensorData):
    """Container for raw sensor data

    This includes, in order, with respective structure:
        - pressure : UInt32
        - acceleration : Acceleration
        - magnetic : Magnetic
        - angular_vel : AngularVelocity
        - euler_angles : EulerAngles
        - quaternion : Quaternion
        - linear_acceleration: LinearAcceleration
        - gravity_vector: LinearAcceleration
        - temperature: Int8
    """

    def __init__(self, data=None):
        """Initializes the RawSensorData object"""
        data = [0] * 24 if data is None else data
        SensorData.__init__(self, data)

    def load(self, data, convert=False):
        SensorData.load(self, data, convert=False)

class CoordinatesWithStatus(ByteStructure):
    """Container for coordinates in x, y, and z (in mm)."""
    byte_size = 13
    data_format = 'iiiB'

    def __init__(self, x=0, y=0, z=0, status=0):
        """Initializes the XYZ or XYZ-derived object."""
        self.data = [x, y, z, status]

    def load(self, data, convert=False):
        self.data = data

    def __str__(self):
        return f'STATUS: {self.status}, X: {self.x}, Y: {self.y}, Z: {self.z}'

    @property
    def x(self):
        return self.data[0]
    
    @property
    def y(self):
        return self.data[1]
    
    @property
    def z(self):
        return self.data[2]
    
    @property
    def status(self):
        return self.data[3]

    @x.setter
    def x(self, value):
        self.data[0] = value

    @y.setter
    def y(self, value):
        self.data[1] = value

    @z.setter
    def z(self, value):
        self.data[2] = value

    @status.setter
    def status(self, value):
        self.data[3] = value

    def to_dict(self):
        return {
            "x": self.x,
            "y": self.y,
            "z": self.z,
        }

class RangeInformation(ByteStructure):
    byte_size = 6
    data_format = 'HI'

    def __init__(self, device_id=0, distance=0):
        self.data = [device_id, distance]

    def load(self, data, convert=0):
        self.data = data

    def __str__(self):
        return "0x{:04x}: {} mm".format(self.device_id, self.distance)

    @property
    def device_id(self):
        return self.data[0]

    @device_id.setter
    def device_id(self, value):
        self.data[0] = value

    @property
    def distance(self):
        return self.data[1]

    @distance.setter
    def distance(self, value):
        self.data[1] = value

class PositioningData(ByteStructure):
    SENSOR_ORDER = [
        CoordinatesWithStatus, Acceleration, AngularVelocity, Magnetic, EulerAngles, 
        Quaternion,LinearAcceleration, Acceleration, Pressure, MaxLinearAcceleration
                    ]

    def __init__(self, flags):
        self.data = [0]
        self.flags = flags
        self.containers = []
        self.byte_size = 0
        self.data_format = ''
        self.amount_of_ranges = 0

        self.set_data_structures()

    def load(self, data, convert=0):
        self.data = data
        data_index = 0
        for container in self.containers:
            data_length = len(container.data_format)
            container.load(data[data_index:data_index + data_length])
            data_index += data_length

    def add_sensor(self, sensor_class):
        self.byte_size += sensor_class.byte_size
        self.data_format += sensor_class.data_format
        self.containers.append(sensor_class())

    def set_data_structures(self):
        self.containers = []
        self.byte_size = 0
        self.data_format = ''

        for index, sensor in enumerate(self.SENSOR_ORDER):
            if self.flags & (1 << index):
                self.add_sensor(sensor)

        if self.has_ranges():
            self.byte_size += 1
            self.data_format += 'B'
            self.containers.append(SingleRegister())

    def set_amount_of_ranges(self, amount_of_ranges):
        self.amount_of_ranges = amount_of_ranges
        if self.has_ranges():
            [self.add_sensor(RangeInformation) for i in range(amount_of_ranges)]

    def has_ranges(self):
        return self.flags & (1 << 15)

    def load_bytes(self, byte_data):
        """Loads a hex byte array in the structure's data"""
        self.byte_data = byte_data
        self.bytes_to_data()

    @property
    def number_of_ranges(self):
        if self.has_ranges():
            return self.amount_of_ranges
        else:
            raise ValueError("Ranges not given as a flag")

    def __str__(self):
        return 'TODO'

if __name__ == "__main__":
    # shortcut to not have to find out the port yourself
    serial_port = get_first_pozyx_serial_port()
    if serial_port is None:
        warn("No Pozyx connected. Check your USB cable or your driver!")
        quit()

    # enable to send position data through OSC
    use_processing = True

    # necessary data for calibration, change the IDs and coordinates yourself according to your measurement
    anchors = [DeviceCoordinates(0x765c, 1, Coordinates(129, -88, 660)),
               DeviceCoordinates(0x763b, 1, Coordinates(1535, 2113, 660)),
               DeviceCoordinates(0x760a, 1, Coordinates(2965, 834, 1040)),
               DeviceCoordinates(0x764d, 1, Coordinates(2968, -54, 660))]

    # positioning algorithm to use, other is PozyxConstants.POSITIONING_ALGORITHM_TRACKING
    # positioning dimension. Others are PozyxConstants.DIMENSION_2D, PozyxConstants.DIMENSION_2_5D
    # height of device, required in 2.5D positioning
    pozyx = PozyxSerial(serial_port)

    pozyx.setUWBSettings(UWBSettings(5,0,2,0x08,11.5))
    localization = ReadyToLocalize(PozyxSerial(serial_port), None, anchors, PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY, PozyxConstants.DIMENSION_3D, 1000, None)
    localization.setup()
    while True:
        """irgendwie Tastenabfrage einbauen"""
        localization.loop()
